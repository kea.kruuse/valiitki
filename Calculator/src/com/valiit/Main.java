package com.valiit;

public class Main {

    public static void main(String[] args) {
        int sum = sum(4, 5);
        System.out.printf("Arvude 4 ja 5 summa on %d%n", sum);
        System.out.printf("Arvude -12 ja -6 vahe on %d%n", subtract(-12, -6));
        System.out.printf("Arvude -12 ja -6 korruts on %d%n", multiply(-12, -6));

        int[] numbers = new int[3];
        numbers[0] = 1;
        numbers[1] = 2;
        numbers[2] = -2;

        sum = sum(numbers);
        System.out.printf("Massiivi arvude summa on %d%n", sum);

        numbers = new int[] { 2, 5, 12, -12};
        sum = sum(new int[] {2, 3, 12, 323, 43, 434, -11});
        System.out.printf("Massiivi arvude summa on %d%n", sum);
        sum = sum(new int[] {2, 3});

        int[] reversed = reverseNumbers(numbers);

        printNumbers(reversed);

        printNumbers(new int[] {1, 2, 3, 4, 5});

        printNumbers(reverseNumbers(new int[] {1, 2, 3, 4, 5}));

        printNumbers(convertToIntArray(new String[] { "2", "-12", "1", "0", "17" }));

        String[] numbersAsText = new String[] { "200", "-12", "1", "0", "17" };
        int[] numbersAsInt = convertToIntArray(numbersAsText);
        printNumbers(numbersAsInt);

        double radius = 10.25;
        System.out.printf("Ringi raadiusega %.2f ümbermõõt on %.2f%n", radius, circumference(radius));

        int a = 23;
        int b = 80;
        // Kui tahame kasutada % märki printf sees, siis kasutame topelt % ehk %%
        System.out.printf("Arv %d moodustab arvust %d: %.2f%%%n",a, b, percent(a, b));

        System.out.printf("Masiivi arvude keskmine on %.2f%n", average(new int[] {1, 2, 3, 4}));

        System.out.printf("Sõnade massiiv liidetuna sõnadeks on %s%n",
                arrayToString(new String[] {"Elas", "metsas", "Mutionu"}));
        System.out.printf("Sõnade massiiv liidetuna sõnadeks on %s%n",
                arrayToString(" ja ",new String[] {"Elas", "metsas", "Mutionu"}));

    }

    // Meetod, mis liidab 2 täisarvu kokku ja tagastab nende summa
    static int sum(int a, int b) {
        int sum = a + b;
        return sum;
    }

    // subract
    static int subtract(int a, int b) {
        return a - b;
    }

    // multiply
    static int multiply(int a, int b) {
        return a * b;
    }

    // divide
    static double divide(int a, int b) {
        return (double)a / b;
    }

    // Meetod, mis võtab parameetriks täisarvude massiivi ja liidab elemndid kokku ning tagastab summa
    static int sum(int[] numbers) {
        int sum = 0;

        for (int i = 0; i < numbers.length; i++) {
            sum += numbers[i];
        }
        return sum;
    }

    // Meetod, mis võtab parameetriks täisarvude massivi, pöörab tagurpidi ja tagastab selle
    // 1 2 3 4 5
    // 5 4 3 2 1
    static int[] reverseNumbers(int[] numbers) {
        int[] reversedNumbers = new int[numbers.length];

        for (int i = 0; i < numbers.length; i++) {
            reversedNumbers[i] = numbers[numbers.length - i - 1];
        }

        return  reversedNumbers;
    }

    // Meetod, mis prindib välja täisarvude massiivi elemendid
    static void printNumbers(int[] numbers) {
        for (int i = 0; i < numbers.length; i++) {
            System.out.println(numbers[i]);
        }
    }

    // Meetod, mis võtab parameetiks stringi masiivi (eeldusel, et tegeilult seal massiivis on
    // numbrid stringidena) ja teisendab numbrite massiiviks ning tagastab selle
    static int[] convertToIntArray(String[] numbersAsText) {
        int[] numbers = new int[numbersAsText.length];

        for (int i = 0; i < numbers.length; i++) {
            numbers[i] = Integer.parseInt(numbersAsText[i]);
        }
        return numbers;
    }

    // Meetod, mis võtab parameetriks stringi massiivi ning tagastab lause,
    // kus iga stringi vahel on tühik
    static String arrayToString(String[] sentence) {
        return String.join(" ", sentence);
    }

    // Meetod, mis võtab parameetriks stringi massiivi ning teine parameeter on sõnade eraldaja
    // Tagastada lause.
    // Vaata eelmiset ülesannet, lihtsalt tühiku asemel saad ise valida, mille pealt sõnu eraldab
    static String arrayToString(String delimiter, String[] sentence) {
        return String.join(delimiter, sentence);
    }

    // Meetod, mis leiab numbrite massiivist keskmise ning tagastab selle
    static double average(int[] numbers) {
        return sum(numbers) / (double)numbers.length;

    }
    // Meetod, mis arvutab mitu % moodustab esimene arv teisest.
    static double percent(int a, int b) {
        return a / (double)b * 100;
    }
    // Meetod, ringi ümbermõõdu raadiuse järgi
    static double circumference(double radius) {
        return 2 * Math.PI * radius;
    }

}
