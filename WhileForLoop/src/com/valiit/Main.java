package com.valiit;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
	// võrdleme for ja while tsükleid

    for (int i = 1; i < 6; i++) {
            System.out.println(i);
    }

    System.out.println();
    // prindi while-iga numbrid 1, 2, 3, 4, 5

    int i = 0;
    do {
        i++;
        System.out.println(i);
    }
    while(i < 5);

    // Kolmas variant
    while(i <= 5) {
        System.out.println(i);
        i++;
    }
    // Küsi kasutajalt, mis päev täna on ja, kuni ta ära arvab.

    Scanner scanner = new Scanner(System.in);
    String day = "";
//    while (!day.toLowerCase().equals("neljapäev")) {
//        System.out.println("Mis päev täna on?");
//        day = scanner.nextLine();
//    }

        for (;!day.toLowerCase().equals("neljapäev");) {
            System.out.println("Mis päev täna on?");
            day = scanner.nextLine();
        }
    // for tsüklik on lihtsalt kaks komponenti juures.
    // kui neid kahte ei täida, siis ongi nagu while tsükkel.
    }
}
