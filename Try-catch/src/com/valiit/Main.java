package com.valiit;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
	// 0-ga jagamise exception

        try {
            int a = 0;
            try {
                int b = 4 / a;
                String word = null;
                word.length(); // tühjusest ei saa pikkust kutsuda. Siis tuleks NullpointerException.
                 // selle kohani kood ei jõua. Kui viga tekib, hüpatakse kohe catch-i.
                // seega kui tahad olla täpne, võiks olla iga rida try-catchis olla.
            }

              //  System.out.println("Esines viga. Ei saa toimingut lõpuni viia.");
            //}
            // Põhimõte peaks olema selline, et nulliga jagada ei saa. ArithmeticException
            // Kui tahaks selle kinni püüda ja öelda kasutajale, et nulliga ei saa jagada, siis saab seda teha try-catch-ga.
            // Ümbritsetud võiks olla väike osa koodist.
            // Exception kui üldine tüüp on klass, millest kõik exceptioni eri tüübid pärinevad, mis omakorda
            // tähendab, et püüdes kinni selle üldise Exceptioni, püüame kinni kõik exceptionid.
            // See tähendab omakorda, et kasutajani ei jõua ükski pärisviga.
            // võib teha ka mitu catch blokki. Lihtsalt järjest kirja ja võib konkretiseerida.
            // saab pärilust kasutada minnec catch-ga konkreetsemast järjest vähem konkreetsema exceptionini
            // kui on mitu catch blokki, siis otsib ta esimese ploki, mis oskab antud Exceptioni kinni püüda
            // oluline on järjekord. Kui oleks nt exception ees, siis ütleks kohe exceptioni veateate.
            catch (ArithmeticException e) {
                if (e.getMessage().equals("/ by zero")) {
                    System.out.println("Nulliga ei saa jagada.");
                } else {
                    System.out.println("Esines artimeetiline viga");
                }
            }
            catch (RuntimeException e) {
                System.out.println("Reaalajas esineb viga.");
            }
            catch (Exception e) {
                ///catch (Exception e) {
                // e tähendab exceptioni nime.
                // e-l on mitmeid huvitavaid asju nt
                e.getMessage(); // saab küsida otse veateksti. Nüüd oleneb, kas tahan seda kasutajale näidata. Logifaili puhul sobib see aga hästi.
                //
                System.out.println("Esines viga. Ei saa toimingut lõpuni viia.");
            }
            // Tavaliselt tehakse nii, et konkreetsemad ettepoole ja siis lõppu ikkagi alati exceptioni.

            // Küsime kasutajalt numbri ja kui number ei ole korrektne, siis ütleme mingi veateate.

            Scanner scanner = new Scanner(System.in);

            boolean correctNumber;

            do {
                correctNumber = false;
                System.out.println("Sisesta number.");
                try {
                    int p = Integer.parseInt(scanner.nextLine());
                    correctNumber = true;
                    //break; - see oleks booleani alternatiiv
                    // Kui oli vale, küsi uuesti.
                } catch (NumberFormatException e) {
                    System.out.println("Number pole õiges formaadis.");
                }
            } while(!correctNumber); // booleaniga saab, break-ga saaks ka, aga seda vältida.

            // Loo täisarvude massiic 5 täisarvuga ning ürita sinna lisada kuues täisarv
            // Näita veateadet

            try {
                int[] massiiv = new int [5];
                massiiv [0] = 1;
                massiiv [1] = 2;
                massiiv [3] = 4;
                massiiv [8] = 77;
                massiiv [20] = 0;
            } catch (ArrayIndexOutOfBoundsException e) {
                System.out.println("Indeks, kuhu tahtsid väärtust määrata, ei eksisteeri.");
            }
        } catch (Exception e) {
            System.out.println("Viga!");
            // Võiks ka kogu koodi panna if-catch sisse, siis on kindel, et midagi ei lähe nö läbi sõrmede.
            // Üldiselt kogu kood pannakse try sisse.

        }
    }
}
