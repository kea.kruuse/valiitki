package com.valiit;

import java.util.ArrayList;
import java.util.List;

enum Fuel {
    GAS,
    PETROL,
    HYBRID,
    ELECTRIC,
    DIESEL
}

public class Car {
    private String maker;
    private String model;
    private int year;
    private Fuel fuel;
    private boolean isUsed;

    private Person driver;
    private Person owner;

    private boolean isEngineRunning;
    private int speed;
    private int maxSpeed;
    private List<Person> passengers = new ArrayList<Person>();
    private int maxPassangers;

    // Konstruktor (constructor) on eriline meetod, mis käivitatakse klassist objekti loomisel
    // Siia saab panna vaikeväärtused
    public Car() {
        System.out.println("Loodi auto objekt.");
        maxSpeed = 200;
        isUsed = true;
        fuel = Fuel.PETROL;
    }

    // sarnaselt meetod overloadinguga saab teha ka constructor overloading



    public Car (String maker, String model, int year, Fuel fuel, boolean isUsed, int maxSpeed, int maxPassangers) {
        this.maker = maker;
        this.model = model;
        this.year = year;
        this.fuel = fuel;
        this.isUsed = isUsed;
        this.maxSpeed = maxSpeed;
        this.maxPassangers = 5;
    }

    // Lisa autole max reisijate arv
    // Lisa autole võimalus hoida reisijaid
    // Lisa meetodid reisijate lisamiseks ja eemaldamiseks autost
    // Kontrolli ka, et ei lisataks rohkem reisijaid, kui mahub

    public List<Person> getPassengers() {
        return passengers;
    }

    public void setPassengers(Person reisija) {
        if (passengers.size() >= maxPassangers){
            System.out.println("Autosse rohkem reisijaid ei mahu.");
        }
        else {
            if (passengers.contains(reisija)){
                System.out.printf("Autos juba on reisija %s.%n", reisija.getFirstName());
                return;
            }
            this.passengers.add(reisija);
            System.out.printf("Autosse on lisatud reisija %s.%n", reisija.getFirstName());
        }
    }

    public void removePassenger(Person passenger) {
        if(passengers.indexOf(passenger) != -1){
            passengers.remove(passenger);
        }
        else {
            System.out.println("Autos pole sellist reisijat.");
        }
    }

    public void showPassengers() {
        // uus tsükkel, mis mõeldud kollektsioonide läbimiseks
        // selle nimi on for-each tsükkel
        // iga elemendi kohta listis passengers tekita objekt passenger
        // 1. kordus Person passenger on esimene reisija
        // 1. kordus Person passenger on teine reisija jne
        // for-each tsüklit mujal ei saagi kasutada. see on ainult listide läbimiseks.

        for(Person passenger : passengers){
            System.out.println("Autos on reisija: " + passenger.getFirstName());
        }
    }

    public String getMaker() {
        return maker;
    }

    public void setMaker(String maker) {
        this.maker = maker;
    }


    public void startEngine() {

        if (!isEngineRunning){

            isEngineRunning = true;
            System.out.println("Mootor käivitus.");

        }
        else {
            System.out.println("Mootor juba töötab.");
        }
    }

    public void stopEngine() {

        if(isEngineRunning){
            isEngineRunning = false;
            System.out.println("Mootor lülitati välja.");
        }
        else {
            System.out.println("Mootor juba seisab.");
        }
    }


    public void accelerate(int targetSpeed){
        if(!isEngineRunning){
            System.out.println("Auto mootor ei tööta, ei saa kiirendada.");
        }
        else if (targetSpeed > maxSpeed){
            System.out.println("Auto nii kiiresti ei sõida");
            System.out.printf("Auto maksimumkiirus on %d%n", maxSpeed);
        }
        else if (targetSpeed < 0) {
            System.out.println("Kiirus ei saa olla alla nulli");
        }
        else if (targetSpeed < speed){
            System.out.println("Kiirendada ei saa alla praeguse kiiruse.");
        }
        else {
            this.speed = targetSpeed;
            System.out.printf("Auto kiirendas kuni kiiruseni %d%n", targetSpeed);
        }
    }
    public void slowDown (int targetSpeed){ //Kodutöö
        if(!isEngineRunning){
            System.out.println("Auto mootor ei tööta, ei saa aeglustada.");
        }
        else if (targetSpeed > maxSpeed){
            System.out.println("Autot ei saa nii aeglustada. Kiirus peab olema alla praeguse kiiruse.");
        }
        else if (targetSpeed < 0) {
            System.out.println("Aeglustada ei saa alla 0 km/h.");
        }
        else if (targetSpeed > speed){
            System.out.println("Aeglustada ei saa üle praeguse kiiruse.");
        }
        else {
            this.speed = targetSpeed;
            System.out.printf("Auto aeglustas kuni kiiruseni %d%n", targetSpeed);
        }
    }

    public void park() { // Kodutöö
        if (speed > 0){
            System.out.println("Autot ei saa parkida enne kui auto seisab.");
        }
        if (speed == 0){
            if (isEngineRunning){
                isEngineRunning = false;
                System.out.println("Mootor seisatati.");
                System.out.println("Auto pargiti.");
            }
            else {
                System.out.println("Auto pargiti.");
            }
        }
    }

    public Person getDriver() {
        return driver;
    }

    public void setDriver(Person driver) {
        this.driver = driver;
    }

    public Person getOwner() {
        return owner;
    }

    public void setOwner(Person owner) {
        this.owner = owner;
    }
}
