package com.valiit;

import java.util.List;

public class Main {

    public static void main(String[] args) {

        Car bmw = new Car();

        bmw.startEngine();
        bmw.startEngine();

        System.out.println();

        bmw.stopEngine();
        bmw.stopEngine();

        System.out.println();

        bmw.accelerate(100);

        bmw.startEngine();

        bmw.accelerate(100);

        System.out.println();

        Car fiat = new Car();
        Car mercedes = new Car();
        System.out.println();

        Car opel = new Car("Opel", "Vectra", 2017, Fuel.ELECTRIC, true, 205, 5);

        opel.startEngine();
        opel.accelerate(200);
        opel.slowDown(0);
        opel.park();

        // seo Person autoga
        // Prindi välja auto omaniku vanus

        Person ownerOne = new Person("Kea", "Kruuse", Gender.FEMALE, 23);
        Person driverOne = new Person("Juku", "Martens", Gender.MALE, 80);
        Person passengerOne = new Person("Piia", "Paula", Gender.FEMALE, 24);
        Person passengerTwo = new Person("Kala", "Mukkel", Gender.MALE, 3);

        opel.setDriver(driverOne);
        opel.setOwner(ownerOne);

        System.out.println();
        System.out.printf("Sõiduki %s juhi vanus on %d aastat.%n", opel.getMaker(), opel.getDriver().getAge());

        System.out.println();
        opel.setPassengers(passengerOne);

        System.out.println();

        System.out.println("Auto reisija on: " + opel.getPassengers().get(0).getFirstName());

        System.out.println();

        opel.showPassengers();

        opel.removePassenger(passengerOne);
        opel.setPassengers(passengerTwo);
        opel.setPassengers(passengerOne);
        opel.setPassengers(ownerOne);
        opel.setPassengers(driverOne);

        System.out.println();
        opel.setPassengers(driverOne);

        System.out.println();
        opel.showPassengers();

    }
}

