package com.valiit;

enum Gender {
    FEMALE,
    MALE,
    NOTSPECIFIED
}

public class Person {

    // Inimene
    private String firstName;
    private String lastName;
    private Gender gender;
    private int age;


    // Kui klassil ei ole defineeritud konstruktorit, siis nähtamatult tegelikult tehakse konstruktor default parameetritega,
    // me lihtsalt ei näe seda, mille sisu on tühi.
    // Kui klassilise ise lisada konstruktor, siis nähtamatute parameetrite konstruktor kustutatakse.
    // Sellest klassist saab teha objekti ainult selle uue konstuktoriga.
    public Person(String firstName, String lastName, Gender gender, int age){ // Konstruktor
        this.firstName = firstName;
        this.lastName = lastName;
        this.gender = gender;
        this.age = age;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}
