package com.valiit;

public class Main {

    public static void main(String[] args) {
	    // Meetodi overriding ehk meetodi ülekirjutamine tähendab seda, et kuskil klassis, millest antud klass pärineb
        // oleva meetodi sisu kirjutatakse pärinevas klassis üle.

        Dog pupper = new Dog();
        pupper.eat();

        Cat kitty = new Cat();
        kitty.setName("Miia");
        kitty.setBreed("Mammut");
        kitty.setWeight(3.3);
        kitty.setAge(2);
        kitty.eat();

        System.out.println();
        kitty.printInfo();

        System.out.println();

        kitty.askForFood("kala");

        // Kirjuta koera getAge üle nii, et kui koeral vanus on null, siis näitaks ikka 1.

        System.out.printf("Koera vanus on %d.%n", pupper.getAge());
        System.out.println();

        pupper.printInfo();

        System.out.println();
        Lion murr = new Lion();
        murr.printInfo();
    }
}
