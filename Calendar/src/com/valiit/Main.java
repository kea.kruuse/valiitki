package com.valiit;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class Main {

    public static void main(String[] args) {

        DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss EEEEE");
        Calendar calendar = Calendar.getInstance();

        calendar.add(Calendar.DATE, -1); // Move calendar to yesterday

        // get current date of calendar which point to the yesterday now
        Date date = calendar.getTime();

        System.out.println(dateFormat.format(date));


        // calendar.add(Calendar.YEAR, 1); // Liigutab aasta võrra edasi

        // date = calendar.getTime();
        //  System.out.println(dateFormat.format(date));

        // Prindi ekraanile selle aasta järele jäänud kuude esimese kuupäeva nädalapäev
        // See kasulik nt, kui tahad meili saata ala iga kuu kolmandal kuupäeval

        DateFormat weekDayFormat = new SimpleDateFormat("EEEEE");

        calendar.set(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), 1); // annab selle aasta selle kuu esimese kuupäeva
        for (int i = calendar.get(Calendar.MONTH); i < 11; i++) {
            calendar.add(Calendar.MONTH, 1);
            date = calendar.getTime();
            System.out.println(weekDayFormat.format(date));
        }


        int currentYear = calendar.get(Calendar.YEAR);

        calendar.add(Calendar.MONTH, 1);

        while (calendar.get(Calendar.YEAR) == currentYear) {

            date = calendar.getTime();
            System.out.println(weekDayFormat.format(date));
            calendar.add(Calendar.MONTH, 1);


        }
    }
}
