package com.valiit;

public class Main {

    public static void main(String[] args) {
        // Elemendid, mida on kuus
        int[] numbers = new int[] {-8, 4, 10, 2, -80, 91, 4};
        boolean oddNumbers = false;

        // võta esimene element, mis siis -2
        // altenratiiv int max = Integer.MIN_VALUE;
        int max = numbers [0];
        // kahekaupa hakatakse vaatama, mis on suurem
        for (int i = 0; i < numbers.length; i++) {
            if (numbers[i] > max){
                max = numbers[i];
            }
        }

        System.out.println(max);

        System.out.println();

        // miinimum
        int min = numbers [0];
        // kahekaupa hakatakse vaatama, mis on suurem
        for (int i = 1; i < numbers.length; i++) {
            if (numbers[i] < min){
                min = numbers[i];
            }
        }
        System.out.println(min);
        System.out.println();

        // leia suurim paaritu arv
        // mitmes number see nimekiras on?

        max = Integer.MIN_VALUE;

        int position = -1;
        for (int i = 1; i < numbers.length; i++) {
            if (numbers[i] > max && numbers[i] % 2 != 0){
                max = numbers[i];
                position = i + 1;
                oddNumbers = true;
            }
        }

        if(oddNumbers){
            System.out.printf("Suurim paaritu arv on %d ja ta on järjekorras number %d.%n", max, position);
        }
        // Kui paarituid arve üldse ei ole, prindi, et paaritud arvud puuduvad.
        else{
            System.out.println("Paaritud arvud puuduvad.");
        }

        // alternatiiv oleks kasutada booleani asemel positionit ehk if(position != -1)

    }
}
