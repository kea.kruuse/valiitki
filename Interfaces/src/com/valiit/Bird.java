package com.valiit;

public class Bird implements Flyer { // bird klass kasutab (implenteerib) liidest (interface'i) Flyer
    @Override
    public void fly(){
        jumpUp();
        System.out.println("Lind lendab.");
        System.out.println();
    }

    private void jumpUp(){
        System.out.println("Hüppab õhku.");
    }
}
