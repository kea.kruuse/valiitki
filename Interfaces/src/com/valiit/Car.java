package com.valiit;

public class Car extends Vehicle implements Driver {
    @Override
    public int getMaxDistance() {
        return 800;
    }

    @Override
    public void drive(int distance) {
        System.out.println("Mootor tööle.");
        System.out.println("Käik sisse.");
        System.out.println("Annab gaasi.");
        System.out.println("Auto sõidab");
        stopDrivingAfterDistance();
        System.out.println();
    }

    @Override
    public void stopDriving() {
        setDistance(0);
        System.out.println("Käik välja");
        System.out.println("Auto lõpetas sõitmise.");
        System.out.println();
    }

    @Override
    public void stopDrivingAfterDistance() {
        if (getDistance() >= getDistance()) {
            System.out.println("Maksmimumdistants saavutatud.");
            stopDriving();
        }
    }
}
