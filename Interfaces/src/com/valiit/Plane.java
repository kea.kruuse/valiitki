package com.valiit;

public class Plane extends Vehicle implements Flyer, Driver { // bird klass kasutab (implenteerib) liidest (interface'i) Flyer
    @Override
    public int getMaxDistance() {
        return 1000;
    }

    @Override
    public void fly(){
        doChecklist();
        startEngine();
        System.out.println("Lennuk lendab.");
        System.out.println();
    }

    private void doChecklist (){
        System.out.println("Kontrollitakse check-listi.");
    }

    private void startEngine (){
        System.out.println("Mootor käivitus.");
    }


    @Override
    public void drive(int distance) {
        System.out.println("Mootor tööle.");
        System.out.println("Käik sisse.");
        System.out.println("Annab gaasi.");
        System.out.println("Lennuk sõidab");
        stopDrivingAfterDistance();
    }

    @Override
    public void stopDriving() {
        setDistance(0);
        System.out.println("Käik välja");
        System.out.println("Mootorratas lõpetas sõitmise.");
    }

    @Override
    public void stopDrivingAfterDistance() {
        if(getDistance() >= getMaxDistance()){
            System.out.println("Maksimumdistants saavutatud.");
            stopDriving();
        }
    }
}

