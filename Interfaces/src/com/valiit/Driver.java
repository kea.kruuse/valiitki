package com.valiit;

public interface Driver {
    int getMaxDistance();
    void drive(int distance);
    void stopDriving();
    void stopDrivingAfterDistance();
}
