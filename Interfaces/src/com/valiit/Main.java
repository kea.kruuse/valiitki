package com.valiit;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {
	    // Interface'iga saab ära määrata mingi omaduse, mis peab olema seda kasutaval klassil.
       /* Java includes a concept called interfaces. A Java interface is a bit like a class,
       except a Java interface can only contain method signatures and fields.
       An Java interface cannot contain an implementation of the methods, only the signature
       (name, parameters and exceptions) of the method.

        You can use interfaces in Java as a way to achieve polymorphism. I will get back to polymorphism later in this text.*/

       // Interface aitab omaduste järgi grupeerida

        Flyer  linnuke = new Bird();
        linnuke.fly();

        System.out.println();

        Flyer ryanAir = new Plane();
        ryanAir.fly();

        // Samuti saab nii hoida täiesti erinevaid objekte listis

        List<Flyer> flyers = new ArrayList<>();
        flyers.add(linnuke);
        flyers.add(ryanAir);

        Plane boeing = new Plane();
        flyers.add(boeing);

        Bird dove = new Bird();
        flyers.add(dove);

        // Samuti saab nt koos panna lendama

        for (Flyer flyer: flyers
             ) {
            flyer.fly();
        }

        Driver volvo = new Car();
        Driver secondPlane = new Plane();
        Driver newMoto = new Motorcycle();

        List<Driver> vechiclesWithDriver = new ArrayList<>();
        vechiclesWithDriver.add(volvo);
        vechiclesWithDriver.add(secondPlane);
        vechiclesWithDriver.add(newMoto);

        for (Driver drivers: vechiclesWithDriver
        ) {
            drivers.drive(500);
        }


        // Lisa liides Driver, klass Car
        // mõtle, kas mõlemad võiks kasutada Driver liidest
        // võiks sisaldada 3 meetodi definitsiooni/kirjeldust:
        // int getMaxDistance()
        // void drive()
        // void stopDriving()
        // void stopDrivingAfterDistance()
        // kui tehtud, proovi panna auto ja lennuk mõlemad seda liidest kasutama.
        // Lisa mootorratas

    }
}
