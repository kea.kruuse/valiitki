package com.valiit;

public class Motorcycle extends Vehicle implements Driver {
    @Override
    public int getMaxDistance() {
        return 400;
    }

    @Override
    public void drive(int distance) {
        System.out.println("Mootor tööle.");
        System.out.println("Käik sisse.");
        System.out.println("Annab gaasi.");
        System.out.println("Mootorratas sõidab");
        System.out.println();
        stopDrivingAfterDistance();
    }

    @Override
    public void stopDriving() {
        setDistance(0);
        System.out.println("Käik välja");
        System.out.println("Mootorratas lõpetas sõitmise.");
        System.out.println();
    }

    @Override
    public void stopDrivingAfterDistance() {
        if(getDistance()>= getDistance()){
            System.out.println("Rohkem ei saa sõita.");
            stopDriving();
        }
    }
}
