package com.valiit;

public interface Flyer {
    //Interface'il ei panda kirja ei publicut ega private'it.
    // Interface ehk lidies sunnib seda kasutavat e implementeerivat klassi omama
    // liideses kirja pandud meetodeid (sama tagastuse tüübiga ja sama parameetrite kombinatsiooniga.
    void fly();
}
