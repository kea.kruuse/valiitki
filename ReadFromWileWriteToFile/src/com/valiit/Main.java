package com.valiit;

import java.io.*;
import java.nio.charset.Charset;

public class Main {

    public static void main(String[] args) {
        // Loe failist input.txt iga teine rida ning kirjuta need read faili output.txt

        try {
            // Notepad vaikimisi kasutab ansi encodingut.
            // Selleks, et fileReader oskaks seda korrektselt lugeda (täpitähti), peame talle ette ütlema, et loe seda ANSI encodingus.
            // Cp1252 on javas ANSI encoding
            FileReader fileReader = new FileReader("C:\\Users\\opilane\\Documents\\input.txt", Charset.forName("Cp1252")); // arvesta seda, et lugemisel selline encoding
            BufferedReader bufferedReader = new BufferedReader(fileReader);

            // Faili kirjutades on javas vaikeväärtus UTF-8; Charset.forName("UTF-8")
            FileWriter fileWriter = new FileWriter("output.txt", Charset.forName("UTF-8")); //charset

            String line = bufferedReader.readLine();

            int count = 0;

            while (line != null) {
                System.out.println(line);
                line = bufferedReader.readLine();
                count++;

                try {

                    if ((line != null && count %2 != 0)){ // kui tahad iga kolmandat, neljandat jne, siis lihtsalt vt kas jagub kolmega, neljaga jne
                        fileWriter.write(line + System.lineSeparator());
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            fileWriter.close();
            bufferedReader.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}