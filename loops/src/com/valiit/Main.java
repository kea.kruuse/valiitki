package com.valiit;

public class Main {

    public static void main(String[] args) {
        // for tsükkel on selline tsükkel, kus korduste arv on teada
        // nt kui tean, et tahan midagi korrata 3 korda või 10 korda.
// lõpmatu tsükkel
        //      for ( ; ; ) {
        //          System.out.println("Väljas on ilus ilm.");
        //      }
        //shortcut on for +i
        // esimene komponent on int i = 0 - int muutuja, tavaliselt tähistatakse i-ga
        // siin saab luua muutujaid ja neid algväärtustada
        // luuakse täisarv i, mille väärtus hakkab tsükli sees muutuma.
        // teine komponent i<3: mis tingimusele vastavust see for tsükkel kontrollib
        // see on tingimus, mis peab olema tõene, et tsükkel käivituks ja korduks.
        // Kolmas komponent i++ tähendab, et i suureneb ühe võrra ehk mis selle i-ga iga korduse jooksul juhtub.
        // see on tegevus, mida iga tsükli kordusel lõpus korratakse
        // i++ on sama mis i = i + 1 ehk iseenda ühe võrra suurendamine
        // analoogiliselt on olemas ka i-- mis on sama mis i = i - 1 see nö shortcut
        for (int i = 0; i < 3; i++) {
            System.out.println("Väljas on ilus ilm.");
        }
        // Prindi ekraanile numbrid 1 kuni 10
        for (int i = 1; i < 11; i++) {
            System.out.println(i);
        }

        // Prindi ekraanile numbrid 24st 167ni
        for (int i = 24; i < 168; i++) {
            System.out.println(i);
        }
        System.out.println();
        // Prindi ekraanile numbrid 18st 3ni

        for (int i = 18; i > 2; i--) {
            System.out.println(i);
        }
        System.out.println();

        //Prindi ekraanile numbrid 2 4 6 8 10
        // i =  i + 2 => i += 2
        // i = i - 4 => i -= 4
        // i = i * 3 => i *= 3
        // i = i / 2 = i /= 2
        for (int i = 2; i < 11; i = i + 2) {
            System.out.println(i);
        }
        System.out.println();

        // Prindi ekraanile numbrid 10-20 ja 40-60
        // alternatiiv seda teha on if (i == 21){i == 39}
        for (int i = 10; i < 61; i++) {
            if (i < 21 || i > 39){
                System.out.println(i);
            }
        }
        System.out.println();
        // Prindi kõik arvud, mis jaguvad 3ga vahemikus 10 kuni 50
        // Jäägi leidmine a % 3
        for (int i = 10; i < 51; i++) {
            if (i % 3 == 0){
                System.out.println(i);
            }
        }
    }
}
