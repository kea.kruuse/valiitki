package com.valiit;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        // Primitive data types - includes byte , short , int , long , float , double , boolean and char.

        // NonPrimitve DataType ((Reference ehk viittüüp): Non primitive datatypes are those which uses primitive datatype as base like array, enum, class etc.
        // ... They are created by the programmer and are not defined by Java like primitives are.

        // A reference type references a memory location where the data is stored rather than directly containing a value.

        // Kui panna üks primitiiv (value type) tüüpi muutuja võrduma teise muutujaga, siis tegelikult tehakse arvuti mällu
        // uus muutuja ja väärtus kopeeritakse sinna.

        // Sama juhtub ka primitiiv (value type) tüüpi muutuja kaasa andmisel meetodi parameetriks
        // (tegelikult tehakse arvuti mällu uus muutuja ja väärtus kopeeritakse sinna.

        int a = 3;
        int b = a;
        a = 7;
        System.out.println(b);

        increment(b);
        System.out.println(b);

        int[] numbers = new int[] {-5};
        int[] secondNumbers = numbers;

        numbers[0] = 3;

        System.out.println(secondNumbers[0]);

        Point pointA = new Point();
        pointA.x = 10;
        pointA.y = 1;

        Point pointB = pointA;

        pointB.x = 7;

        System.out.println(pointA.x);

        List<Point> points = new ArrayList<>();
        points.add(pointA);
        points.add(pointB);
        System.out.println();

        pointA.y = -4;

        System.out.println(points.get(0).y);
        System.out.println(points.get(1).y);
        System.out.println(pointB.y);

        increment(pointA);
        System.out.println();

        increment(numbers);
        System.out.println();

    }

    public static void increment (int a){
        a++;
        System.out.printf("a on nüüd %d.%n",a);
    }

    // suurendame x ja y koordinaate ühe võrra
    public static void increment (Point point){
        point.x++;
        point.y++;
        System.out.printf("x ja y on nüüd %d ja %d.%n", point.x, point.y);
    }


    // static tähendab, et saab selle niisama välja kutsuda
    // Kui on non-static, siis kuulub selle klassi hulka ehk peab koos objektiga välja kutsuma
    // non-staatikud ei saa staatiliselt välja kutsuda
    // st staatiku jaoks ei pea looma objekti. non-staticu jaoks peab.


    // suurendame kõiki elemente 1 võrra
    public static void increment (int[] numbers){
        for (int i = 0; i < numbers.length; i++) {
            numbers[i]++;
            System.out.printf("Number kohal %d on nüüd väärtusega %d.%n", i , numbers[i]);
        }
    }

}
