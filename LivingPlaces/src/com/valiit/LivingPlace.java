package com.valiit;

public interface LivingPlace {
    // Võta kõik farmi meetodid ja pane interface'i.

    void addAnimal(Animal animal);
    void printAnimalCounts();
    void removeAnimal(String animalType);
}
