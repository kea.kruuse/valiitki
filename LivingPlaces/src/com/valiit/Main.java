package com.valiit;

import java.util.logging.Logger;

public class Main {

    static Logger logger = new Databaselogger();


    public static void main(String[] args) {

        try {
            int =3 / 0;
        } catch (Exception e){
            logger.writeLog("Nulliga jagamise viga");
        }


        LivingPlace livingPlace = new Farm();

        Pig pig = new Pig();

        livingPlace.addAnimal(pig);

        Sheep sheep = new Sheep();

        livingPlace.addAnimal(sheep);
        livingPlace.addAnimal(new Cow());
        livingPlace.addAnimal(new Pig());
        livingPlace.addAnimal(new Pig());
        livingPlace.addAnimal(new Pig());
        livingPlace.addAnimal(new Pig());
        livingPlace.addAnimal(new Pig());

        System.out.println();

        // livingPlace.printOutAnimalsAsList(); - lihtsalt nimekiri

        System.out.println();

        livingPlace.printAnimalCounts();// Meetod, mis prindib välja farmis elavate loomatüüpide arvu

        pig.setName("Kalle");
        sheep.setName("Malle");
        System.out.println();

        // Tee meetod, mis eemaldab farmist looma.

        livingPlace.removeAnimal("Pig");
        System.out.println();

        livingPlace.printAnimalCounts();

        livingPlace.removeAnimal("Pig");
        System.out.println();

        livingPlace.printAnimalCounts();

        livingPlace.removeAnimal("Pig");
        System.out.println();

        livingPlace.printAnimalCounts();

        System.out.println();

        livingPlace.removeAnimal("Pig");
        System.out.println();

        livingPlace.printAnimalCounts();

        System.out.println();

        livingPlace.removeAnimal("Pig");
        System.out.println();

        livingPlace.printAnimalCounts();
        System.out.println();

        livingPlace.removeAnimal("Pig");
        System.out.println();

        livingPlace.printAnimalCounts();


        livingPlace.addAnimal(new Cow());

        // Exeptionite logimine logifaili


    }
}
