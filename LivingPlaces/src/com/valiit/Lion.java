package com.valiit;

public class Lion extends CarnivoreAnimal {
    private boolean hasMane;

    public boolean isHasMane() {
        return hasMane;
    }

    public void setHasMane(boolean hasMane) {
        this.hasMane = hasMane;
    }

    public void rawrd(){
        System.out.println("Raaaawrd!");
    }

    @Override
    public String getName() {
            return "Mina olen lõvikuningas";

    }

}
