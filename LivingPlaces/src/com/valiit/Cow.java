package com.valiit;

public class Cow extends FarmAnimal {
    private int numberOfCalves;

    public int getNumberOfCalves() {
        return numberOfCalves;
    }

    public void setNumberOfCalves(int numberOfCalves) {
        this.numberOfCalves = numberOfCalves;
    }

    public void doMooSound() {
        System.out.println("Moooooooo!");
    }

}
