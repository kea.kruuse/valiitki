package com.valiit;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Forest implements LivingPlace {

    private List<ForestAnimal> animals = new ArrayList<ForestAnimal>();

    // Siin hoitakse infot, palju meil igat looma metsas on
    private Map<String, Integer> animalCounts = new HashMap<String, Integer>();

    // Siin hoitakse infot, palju meil igat looma metsa mahub
    private Map<String, Integer> maxAnimalCounts = new HashMap<String, Integer>();

    public Forest() {
        maxAnimalCounts.put("Wolf", 15);
        maxAnimalCounts.put("Lion", 3);
        maxAnimalCounts.put("Giraffe", 2);
    }

    @Override
    public void addAnimal(Animal animal) {
        if (!ForestAnimal.class.isInstance(animal)) {
            System.out.println("Metsas saavad elada ainult metsikud loomad");
            return;
        }

        String animalType = animal.getClass().getSimpleName();

        if (!maxAnimalCounts.containsKey(animalType)) {
            System.out.println("Metsas sellisele loomale üldse kohta pole.");
            return;
        }

        int maxAnimalCount = maxAnimalCounts.get(animalType);

        int animalCount;

        if (animalCounts.containsKey(animalType)) {
            animalCount = animalCounts.get(animalType);

            if (animalCount >= maxAnimalCount) {
                System.out.println("Metsas on sellele loomale kohad täis.");
                return;
            }

            animalCounts.put(animalType, animalCounts.get(animalType) + 1);

            // Kui else blokki läheb, tähendab, et sellist looma pole farmis.
            // kindlasti sellele loomale kohta on.
        } else {
            // võin julgelt teisendada, sest tean, et ta on FarmAnimal, muidu poleks nii kaugele jõudnud.

            animalCounts.put(animalType, 1);
        }

        animals.add((ForestAnimal) animal);

        System.out.printf("Metsa lisati loom %s%n", animalType);

    }

    @Override
    public void printAnimalCounts() {

        for (Map.Entry<String, Integer> entry : animalCounts.entrySet()) {

            System.out.printf("Looma %s esines %d korda%n", entry.getKey(), entry.getValue());
        }

    }

    @Override
    public void removeAnimal(String animalType) {
        boolean animalFound = false;

        for (ForestAnimal animal : animals) {

            if (animal.getClass().getSimpleName().equals(animalType)) {

                System.out.printf("%s on metsast eemaldatud.%n", animalType);
                animals.remove(animal);

                // Kui see oli viimane loom, eemalda.

                if(animalCounts.get(animalType) == 1){
                    animalCounts.remove(animalType);
                }

                else{
                    // Kui seal on veel selliseid loomi, vähenda.
                    animalCounts.put(animalType, animalCounts.get(animalType) - 1);

                }
                animalFound = true;
                break;
            }
        }
        if(!animalFound) {
            System.out.println("Metsas pole sellist looma.");
        }

    }
}

// Täienda zoo ja forest klasse nii, et neil oleks nende kolme meetodi sisu.