package com.valiit;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Zoo implements LivingPlace {


    private List<ZooAnimal> animals = new ArrayList<ZooAnimal>();

    // Siin hoitakse infot, palju meil igat looma metsas on
    private Map<String, Integer> animalCounts = new HashMap<String, Integer>();

    // Siin hoitakse infot, palju meil igat looma metsa mahub
    private Map<String, Integer> maxAnimalCounts = new HashMap<String, Integer>();

    public Zoo() {
        maxAnimalCounts.put("Wolf", 4);
        maxAnimalCounts.put("Lion", 3);
        maxAnimalCounts.put("Giraffe", 2);
    }

    @Override
    public void addAnimal(Animal animal) {
        if (!ZooAnimal.class.isInstance(animal)) {
            System.out.println("Loomaaias saavad elada ainult põnevad loomad");
            return;
        }

        String animalType = animal.getClass().getSimpleName();

        if (!maxAnimalCounts.containsKey(animalType)) {
            System.out.println("Loomaaias sellisele loomale üldse kohta pole.");
            return;
        }

        int maxAnimalCount = maxAnimalCounts.get(animalType);

        int animalCount;

        if (animalCounts.containsKey(animalType)) {
            animalCount = animalCounts.get(animalType);

            if (animalCount >= maxAnimalCount) {
                System.out.println("Loomaaias on sellele loomale kohad täis.");
                return;
            }

            animalCounts.put(animalType, animalCounts.get(animalType) + 1);

        } else {

            animalCounts.put(animalType, 1);
        }

        animals.add((ZooAnimal) animal);

        System.out.printf("Loomaaeda lisati loom %s%n", animalType);

    }

    @Override
    public void printAnimalCounts() {

        for (Map.Entry<String, Integer> entry : animalCounts.entrySet()) {

            System.out.printf("Looma %s esines %d korda%n", entry.getKey(), entry.getValue());
        }

    }

    @Override
    public void removeAnimal(String animalType) {
        boolean animalFound = false;

        for (ZooAnimal animal : animals) {

            if (animal.getClass().getSimpleName().equals(animalType)) {

                System.out.printf("%s on loomaaiast eemaldatud.%n", animalType);
                animals.remove(animal);


                if(animalCounts.get(animalType) == 1){
                    animalCounts.remove(animalType);
                }

                else{
                    animalCounts.put(animalType, animalCounts.get(animalType) - 1);

                }
                animalFound = true;
                break;
            }
        }
        if(!animalFound) {
            System.out.println("Loomaaias pole sellist looma.");
        }

    }
}
