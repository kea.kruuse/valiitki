package com.valiit;

public class Animal {
    private String name;
    private String breed;
    private int age;
    private double weight;
    private boolean isAlive = true;


    public boolean isAlive() {
        return isAlive;
    }

    public void setAlive(boolean alive) {
        isAlive = alive;
    }

    public String getName() {
            return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBreed() {
        return breed;
    }

    public void setBreed(String breed) {
        this.breed = breed;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public void eat() {
        System.out.println("Söön toitu.");
    }

    public void printInfo() {
        System.out.println("Info: ");
        System.out.printf("Nimi: %s. %n", getName());
        System.out.printf("Tõug: %s. %n", getBreed());
        System.out.printf("Vanus: %d aastat. %n", getAge());
        System.out.printf("Kaal: %.1f kg. %n", getWeight());
        System.out.println();
    }



}
