package com.valiit;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Farm implements LivingPlace {
    //Siin hoidakse loomi
    private List<FarmAnimal> animals = new ArrayList<FarmAnimal>();

    // Siin hoitakse infot, palju meil igat looma farmis on
    private Map<String, Integer> animalCounts = new HashMap<String, Integer>();

    // Siin hoitakse infot, palju meil igat looma farmis mahub
    private Map<String, Integer> maxAnimalCounts = new HashMap<String, Integer>();

    // kirjutamata reegel: kõik nimekirjad on mitmuses.

    public Farm() {
        maxAnimalCounts.put("Pig", 5);
        maxAnimalCounts.put("Cow", 3);
        maxAnimalCounts.put("Sheep", 15);
    }

    @Override
    public void addAnimal(Animal animal) {

        // Kas animal on tüübist FarmAnimal või pärineb sellest tüübist
        // isInstance tähendab, et kas ta on Farmanimal või pärineb sellest

        if (!FarmAnimal.class.isInstance(animal)) {
            System.out.println("Farmis saavad elada ainult farmiloomad");
            return;
        }

        String animalType = animal.getClass().getSimpleName();

        if (!maxAnimalCounts.containsKey(animalType)) {
            System.out.println("Farmis sellisele loomale üldse kohta pole.");
            return;
        }

        int maxAnimalCount = maxAnimalCounts.get(animalType);

        int animalCount;

        if (animalCounts.containsKey(animalType)) {
            animalCount = animalCounts.get(animalType);

            if (animalCount >= maxAnimalCount) {
                System.out.println("Farmis on sellele loomale kohad täis.");
                return;
            }

            animalCounts.put(animalType, animalCounts.get(animalType) + 1);

            // Kui else blokki läheb, tähendab, et sellist looma pole farmis.
            // kindlasti sellele loomale kohta on.
        } else {
            // võin julgelt teisendada, sest tean, et ta on FarmAnimal, muidu poleks nii kaugele jõudnud.

            animalCounts.put(animalType, 1);
        }

        animals.add((FarmAnimal) animal);

        System.out.printf("Farmi lisati loom %s%n", animalType);
    }

    public void printOutAnimalsAsList() {

        String listOfAnimals = "";

        for (int i = 0; i < animals.size(); i++) {

            if (i == animals.size() - 1) {
                listOfAnimals = listOfAnimals + animals.get(i).getClass().getSimpleName() + ".";
            } else {
                listOfAnimals = listOfAnimals + animals.get(i).getClass().getSimpleName() + ", ";
            }
        }

        System.out.println("Farmis on järgmised loomad: " + listOfAnimals);
    }

    @Override
    public void printAnimalCounts() {

        for (Map.Entry<String, Integer> entry : animalCounts.entrySet()) {

            System.out.printf("Looma %s esines %d korda%n", entry.getKey(), entry.getValue());
        }

    }

    @Override
    public void removeAnimal(String animalType) {

        boolean animalFound = false;

        for (FarmAnimal animal : animals) {

            if (animal.getClass().getSimpleName().equals(animalType)) {

                System.out.printf("%s on farmist eemaldatud.%n", animalType);
                animals.remove(animal);

                // Kui see oli viimane loom, eemalda.

                if(animalCounts.get(animalType) == 1){
                    animalCounts.remove(animalType);
                }

                else{
                    // Kui seal on veel selliseid loomi, vähenda.
                    animalCounts.put(animalType, animalCounts.get(animalType) - 1);

                }
                animalFound = true;
                break;
            }
        }
        if(!animalFound) {
            System.out.println("Farmis pole sellist looma.");
        }
    }
}
