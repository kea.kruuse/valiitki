﻿-- See on lihtne hello world teksti päring, mis tagastab ühe rea ja ühe veeru
-- veerul puudub pealkiri. Selles veerus ja reas on tekst Hello World.

/*
SQL (Structured Query Language) is a database management language for relational databases.
SQL itself is not a programming language, but its standard allows creating procedural extensions for it,
which extend it to functionality of a mature programming language.

SQL is an exceptional reason programming language that is utilized to interface with databases.
It works by understanding and analyzing databases that include data fields in their tables.
For example, we can take a large organization where a lot of data have to be stored and managed.

Relatsioonimudeli puhul on objektid andmebaasis ja nendevahelised seosed esitatud tabelite kujul.
Need võivad koosneda enamast kui ühest tabelist, mis on omavahel seotud.
Seostamine tähendab andmefailide ühendamist ühesuguse sisuga väljade järgi.
Relatsioonandmebaas koosneb nimega tabelitest, kus on nimega veerge üks või enam, ning suvaline arv ridu.
Ühes andmebaasis võib olla mitmeid tabeleid. Iga selline tabel kujutab endast üht relatsiooni.
Lisatingimuseks on, et üheski relatsioonis ei või olla kahte ühesugust rida.
Iga tabeli kohta võime seega määrata ühe või mitu veergu, mille väärtuste kaudu on read identifitseeritavad.
Taolist veergude kogumit nimetatakse primaarvõtmeks. Primaarvõtmete või lihtsalt võtmete järgi võime ühendada eri tabelite andmeid.
Näiteks tabel "Töötajad" võib sisaldada veergu "Asukoht", sisaldades väärtust, mis sobib tabeli "Asukoht" võtmega.

Kuna tabelid on üksteisest sõltumatud, muudab see relatsioonimudelil põhinevad andmed baasis ettenägematute muutuste ja vajaduste suhtes paindlikeks.
Oma paindlikkuse tõttu on relatsioonimudelid muid alammudeleid suhteliselt kiiresti välja tõrjumas.
*/

SELECT 'Hello World';

SELECT 3;
-- Täisarvu saab küsida ilma kommentaarideta.

SELECT 'RAUD' + 'TEE' -- ei tööta PostgreSQL'is. Näiteks MSSql'is töötab.

-- Standard CONCAT töötab kõigis erinevates SQL serverites
SELECT CONCAT('raud','tee', 2, 2, 3, 0, 4); -- nii on stringide liitmine

-- punktiga saba teha uue veeru
SELECT 'Peeter', 'Paat', 23, 75.45, 'Blond'

-- AS märksõnaga saab anda antud veerule nimega

SELECT
	'Peeter' AS Eesnimi,
	'Paat' AS Perekonnanimi,
	23 AS vanus,
	75.45 AS kaal,
	'Blond' AS juuksevärv
	
SELECT NOW(); -- tagastab praeguse kellaaja ja kuupäeva vaikimisi formaadis

-- Kui tahan konkreetset osa sellest, näiteks aastat
SELECT date_part ('year', NOW());
SELECT date_part ('month', NOW());
SELECT date_part ('day', NOW());

-- kuupäeva formaatimine eesti kuupäeva formaati

--- http://www.postgresqltutorial.com/postgresql-data-types/
-- PostgreSQL provides three primary character types: character(n) or char(n), character varying(n) or varchar(n), and text, where n is a positive integer.
-- kasuta võimalikult väikest, nt nime puhul varchar(32) oleks sobiv

-- interval laseb lisada v eemaldada mingit ajaühikut
SELECT now() + interval '1 day ago';
-- või
SELECT now() - interval '1 day';

SELECT NOW() - INTERVAL '7 years';

SELECT NOW() + interval ' 2 centuries 3 years 2 months 1 weeks 3 days 4 seconds';

-- https://www.uuidgenerator.net/
--- A universally unique identifier (UUID) is a 128-bit number used to identify information in computer systems.
-- The term globally unique identifier (GUID) is also used, typically in software created by Microsoft.

-- käsud on suurte tähtedega ja nimed väikestega ja alakriipsuga ehk nt SET minu_tabel

-- tabeli loomine:

CREATE TABLE student (
	id serial PRIMARY KEY, -- MySQL-i puhul id int serial_increment on süntaks, aga tundub, et nii tark, et sööb ka seda.
	-- serial tähendab, et tüübiks on int, mis hakkab suurenema ühe võrra
	-- primary key tähendab, et see on unikaalne väli tabelis
	first_name varchar(64) NOT NULL,
	last_name varchar(64) NOT NULL,
	height int NULL,
	weight numeric(5, 2) NULL, -- esimene number mitu nr üldse mitte arvestades koma, teine nr mitu kohta p koma
	birthday date NULL -- komad iga rea lõpus va viimane
	-- NULL / NOT NULL tähendab seda, kas peab olema täidetud väli v mitte
);


-- tabelist kõikide ridade ja kõikide veergude küsimine:
-- tärn tähendab, et anna kõik veerud

SELECT * FROM public.student 

-- INFO GENEREERIMINE

SELECT
	first_name AS eesnimi,
	last_name AS perekonnanimi,
	EXTRACT (YEAR FROM age(birthday)) AS vanus,
	date_part('year', NOW()) - date_part('year', birthday) AS vanus -- õpetaja variant
	
	

SELECT CONCAT(first_name, ' ',EXTRACT (YEAR FROM age(birthday)), ' ', last_name) AS täisnimi FROM student;
	


FROM
	student;
	
	SELECT
*
FROM
student
WHERE
height = 160;

---


SELECT
*
FROM
student
WHERE
height = 178 AND -- && asemel AND ja || asemel OR
first_name = 'Peeter';

-- vali kaks

SELECT
*
FROM
student
WHERE
(first_name = 'Peeter' AND last_name = 'Termomeeter') OR
(first_name = 'Mari' AND last_name = 'Maasikas');

-- vali õpilased, kelle pikkus jääb 160-180 vahele

SELECT
*
FROM
student
WHERE
(height < 180 AND height > 170);

-- Anna õpilased, kes pikemad kui 170cm või lühemad kui 150 cm
SELECT
*
FROM
student
WHERE
(height <= 150 OR height >= 170);

-- Anna õpilased, kellel on sünnipäev jaanuaris
SELECT
first_name AS eesnimi,
height AS pikkus
FROM
student
WHERE
EXTRACT(MONTH FROM birthday) = 01;

-- Anna õpilased, kelle birthday on null (määramata)

SELECT
*
FROM
student
WHERE
birthday IS NULL; -- NULLiga võrdlemine ehk kas see on tühi

--

SELECT
*
FROM
student
WHERE
birthday IS NULL AND weight is NOT NULL; 

-- Õpilased, kes ei ole 180cm pikad
SELECT
*
FROM
student
WHERE
height <> 180;

-- veel saab ka nii: NOT (height= 180) või height != 180;

--Õpilased, kelle eesnimi ei ole Peeter, Mari ega Kalle
SELECT
*
FROM
student
WHERE
first_name NOT IN ('Peeter', 'Mari', 'Kalle'); -- oleksi puhul kasutad IN


-- Anna õpilased, kelle sünnikuupäev on kuu esimesel kolmel päeval
SELECT
*
FROM
student
WHERE
date_part ('day', birthday) IN (1, 2, 3)

-- kõik where väärtused jätavad välja null-i

-- Anna mulle õpilased pikkuse jrk lühemast pikemaks
-- Pikkused võrdsed järjest kaalu järgi

SELECT
*
FROM
student
ORDER BY height

-- Kui tahan tagurpidises järk, siis lisandub sõna DESC (Descending)
-- Tegelikult on olemas ka ASC (Ascending), mis on vaikeväärtus

SELECT*
FROM
student
ORDER BYfirst_name DESC, weight ASC, last_name;

-- Anna mulle vanuse järjekorras vanemast nooremaks õpilaste pikkused,
-- mis jäävad 160 ja 170 vahele


-- Anna mulle vanuse järjekorras vanemast nooremaks õpilaste pikkused,
-- mis jäävad 160 ja 170 vahele

SELECT
height AS pikkus
FROM
student
WHERE
height >= 160 AND height <= 170
ORDER BY
birthday DESC

-- K tähega algavad nimed

SELECT
*
FROM
student
WHERE
first_name LIKE 'K%' OR
first_name LIKE '%ee%' -- annab selle, et on sees
first_name LIKE 'er%' -- annab lõpu

-- valude'de sisestamine

INSERT INTO student
	(first_name, last_name, height, weight, birthday)
VALUES
	('Miia', 'Marrak', 167, 59, '1994-03-09'),
	('Maur', 'Kandjad', 158, 70.99, '1994-03-09'),
	('Riho', 'Iubajung', 177, 120.10, '1994-03-09'),
	('Lalo', 'Lalullumull', 70, 29, '2018-10-11'),
	('Jarojar', 'Korogor', 190, 91.80, '1994-11-22')
	
-- Tabelis kirja muutmine
-- UPDATE lausega peab olema ettevaatlik
-- alati peab kasutama WHERE-i lause lõpus
-- muidu juhtub nii, et muduad kõikide perekonnanimed kogemata ära, back-up-i pole ja ei saa kunagi teada, mis nende perekonnanimed tegelikult olid

UPDATE
student
SET
	height = 172
WHERE
	first_name = 'Peeter' AND last_name = 'Termomeeter';
	
	--
	
UPDATE
student
SET
	first_name = 'John',
	weight = 101.02
WHERE
	id = 6
	
-- height ühe võrra suuremaks

UPDATE
student
SET
	height = height +1
WHERE
	id = 6; -- kui siia kriteeriumit ei pane, suurenevad kõik ühe võrra (va null)
	
-- Suurenda hiljem kui 1993 sündinud õpilastel sünnipäeva ühe võrra
	
UPDATE
student
SET
	birthday = birthday + interval '1 days' -- võib panna ka + 0000-00-01, aga see pole hea
WHERE
	EXTRACT(YEAR FROM birthday) > 1993;
	
-- KUSTUTAMISEGA OLLA ETTEVAATLIK
-- ALATI KASUTA WHERE'i

DELETE FROM
student
WHERE
id = 6;

-- saab ka panna midaiganes tingimusi

-- CRUD operations on need
-- Within computer programming, the acronym CRUD stands for create, read, update and delete
-- ehk kui saab andmed lisada, küsida, muuta või kustutada, on CRUD operations
-- ehk põhiasjad, mida saab andmetega teha

-- Loo uus tabel loan, millel on väljad: amount (reaalarv ehk komaga), start date, due date, student id

CREATE TABLE loan(
id serial PRIMARY KEY NOT NULL,
amount numeric(11, 2) NOT NULL,
start_date date NOT NULL,
due_date date NOT NULL,
student_id int NOT NULL)

-- lisa laene õpilastele

INSERT INTO public.loan(
	amount, start_date, due_date, student_id)
	VALUES
	(50, '2019-05-01', '2019-12-12', 1),
	(499, '2019-05-01', '2019-12-12', 1),
	(10000, '2019-03-24', '2050-02-02', 2),
	(4321, '2019-05-11', '2019-04-08', 3),
	(909090, '2016-05-01', '2090-01-01', 4),
	(767, NOW(), '2019-12-12', 4);
	
-- anna mulle kõik õpilased koos laenudega
-- siin ilma laenuta õpilasi pole
SELECT
student.*
FROM
student
JOIN
loan
ON student.id = loan.student_id

-- tavaliselt uurib kes ja palju nad võlgu on

SELECT
student.first_name,
student.last_name,
loan.amount
FROM
student
JOIN
loan
ON student.id = loan.student_id
WHERE
loan.amount > 500
ORDER BY
loan.amount DESC;

-- JOIN-e on erinevat tüüp
-- JOIN, mille ette midagi ei kirjuta, on INNER JOIN
-- INNER JOIN on vaikimisi JOIN, INNER sõna võib ära jätta sarnaselt nagu ASC võib ära jätta
-- INNER JOIN on selline tabelite liitmine, kus liidetakse ainult need read, kus on võrdsed student.id = loan.student_id
-- ehk need read, kus tabelite vahel on seos. Ülejäänud read ignoreeritakse.

-- tabeli muutmineALTER TABLE loan
ADD COLUMN loan_type_id int;

-- tabeli kustutamine
DROP TABLE student
-- sarnaselt ka DROP COLUMN, RENAME COLUMN

-- DEFAULT ja DROP DEFAULT

-- lisame mõned laenutüübid

INSERT INTO loan_type (name, description)
VALUES
('õppelaen', 'see on väga hea laen'),
('smslaen', 'see on väga halb laen'),
('väikelaen', 'see on kõrge intressiga laen'),
('kodulaen', 'see on madala intressiga laen')

-- mitme tabeli ühendamine

SELECT -- select on tegelikult viimane asi, mida tehakse ehk enne FROM ja JOIN ja siis SELECT
s.first_name,
s.last_name,
l.amount,
lt.name,
lt.description
FROM
	student AS s
JOIN 
	loan AS l
	ON s.id=l.student_id
JOIN
loan_type AS lt
ON lt.id = l.loan_type_id

-- võib erinevat pidi ühendada

SELECT
s.first_name,
s.last_name,
l.amount,
lt.name,
lt.description
FROM
	loan_type AS lt
JOIN
	loan AS l
	ON lt.id = l.loan_type_id

JOIN 
	student AS s
	ON s.id=l.student_id

-- LEFT JOIN
SELECT
s.first_name,
s.last_name,

l.amount
FROM
	student AS s
LEFT JOIN -- selles nimekirjas nüüd kõik, sh ka need kes ei võtnud laenu. Kuvad ka neid, kes ei võtnud laenu
	loan AS l
	ON s.id=l.student_id
	
	-- LEFT JOIN puhul võetakse JOINi esimesest (vasakust) tabelist kõik read ning teises tabelis (paremas)näidetakse puuduvatel kohtade null
	-- RIGHT JOIN puhul vastupidine olukord
	-- pmst saab ka järjekorda muuta ja siis pole RIGHT JOIN-i üldse vajagi
	
	SELECT
s.first_name,
s.last_name,

l.amount
FROM
	student AS s
RIGHT JOIN
	loan AS l
	ON s.id=l.student_id
	
	-- SQLITE on sarnane süsteem, aga lihtsam. Seda kasutavad nt Apple ja erinevad appid
	
-- LEFT JOIN puhul on järjekord väga oluline

-- CROSS JOIN on ka olemas

--iseendaga saab ka join-ida
-- nt oletame, et on rahvatantsurühm ja tahame kõiki võimalikke paariliste kombinatsioone
-- see annab kõik kombinatsioonid kahe tabeli vahel 
SELECT
	s.first_name, st.first_name
FROM
student AS s
CROSS JOIN
student AS st
WHERE
	s.first_name != st.first_name
	
	-- FULL OUTER JOIN teeb LEFT ja RIGHT JOIN-i kokku ehk mõlemast tabelist kõik. Nendel ridadel, kus pole vastet, annab null-i
	
	SELECT
	s.first_name, l amount
FROM
student AS s
FULL OUTER JOIN
loan AS l
ON s.id = l.student_id
	
	
	-- Anna mulle kõigi kasutajate perekonnanimed, kes võtsid smslaenu ja on võlgu üle 100 euro
	-- tulemused järjesta laenuvõtja vanuse järgi väiksemast suuremaks
	
SELECT
s.last_name
FROM
student AS s
JOIN
loan AS l
ON s.id = l.student_id
JOIN
loan_type AS lt
ON lt.id = l.loan_type_id
WHERE
lt.name = 'smslaen' AND
l.amount > 100
ORDER BY
s.birthday AS

-- aggregate functions
-- AVG – calculates the average of a set of values.
-- COUNT – counts rows in a specified table or view.
-- MIN – gets the minimum value in a set of values.
-- MAX – gets the maximum value in a set of values.
-- SUM – calculates the sum of values.

-- Keskmise leidmine. Jäetakse välja read, kus height on NULL
SELECT 
	AVG(height)
FROM
	student

-- Palju on üks õpilane keskmiselt laenu võtnud
-- Arvestatakse ka neid õpilasi, kes ei ole laenu võtnud (amount on NULL)
SELECT
	AVG(COALESCE(loan.amount, 0))
FROM
	student
LEFT JOIN
	loan
	ON student.id = loan.student_id	
	 
	 -- näide min, maks, avg, count arvutamisest
SELECT
	ROUND (AVG (COALESCE(l.amount, 0)), 0) AS "Kesmine laenusumma",
	MIN (l.amount) AS "Väikseim laenusumma",
	MAX (l.amount) AS "Suurim laenusumma",
	COUNT(*) AS "Kõikide ridade arv",
	COUNT(l.amount) AS "Laenude arv", -- jäetakse välja read, kus loan.amount on NULL
	COUNT (s.height) AS "Mitmel õpilasel on pikkus kirjas"
FROM
student AS s
LEFT JOIN
loan AS l
ON s.id = l.student_id

-- harjutus: laenusummad, aga õpilase kaupa grupeerituna ehk tahad summad kokku liita
-- kasutades GROUP BY jäävad SELECT päringu jaoks alles vaid need väljad, mis on
-- GROUP BY-s ära toodud ehk antud juhul first_name ja last_name
-- teisi välju saab ainult kasutada agregaatfunktsioonide sees
-- kui tahan selectis mingit välja kasutada, peab ta group-by-s olemas olema VÕI funktsiooni sees
SELECT
	s.first_name AS "Eesnimi",
	s.last_name AS "Perekonnanimi",
	SUM(l.amount) AS "Laenusummad kokku",
	MIN(l.amount) AS "Väikseim laenusumma",
	MAX(l.amount) AS "Suurim laenusumma"
FROM
	student AS s
JOIN -- Kui teha LEFT JOIN, tulevad ka need, kellel pole laene
	loan as l
	on s.id = l.student_id
GROUP BY
	s.first_name, s.last_name
	
	
-- esimene harjutus.
SELECT
	lt.name AS "Laenutüüp",
	SUM(l.amount) AS "Laenusummad kokku",
	MIN(l.amount) AS "Väikseim laenusumma",
	MAX(l.amount) AS "Suurim laenusumma"
FROM
	loan AS l
JOIN
	loan_type AS lt
	ON l.loan_type_id = lt.id
GROUP BY
	lt.name
	
-- harjutus kaks
SELECT
	date_part('year', s.birthday) AS "Sünniaasta",
	SUM(l.amount) AS "Laenusummad kokku",
	MIN(l.amount) AS "Väikseim laenusumma",
	MAX(l.amount) AS "Suurim laenusumma"
FROM
	student AS s
JOIN
	loan AS l
	ON s.id = l.student_id
JOIN
	loan_type AS lt
	ON l.loan_type_id = lt.id
GROUP BY
	s.birthday
	
-- Anna laenude summad grupeerituna õpilase ning laenu tüübi kaupa ala Mari õppelaen summa

SELECT
	s.first_name AS "Eesnimi",
	lt.name AS "Laenutüüp",
	SUM(l.amount) AS "Laenusummad kokku"
FROM
	student AS s
JOIN
	loan AS l
	ON s.id = l.student_id
JOIN
	loan_type AS lt
	ON l.loan_type_id = lt.id
GROUP BY
	s.first_name,
	lt.name
	
-- Anna  laenude sümmad sünniaastate järgi

SELECT
	date_part('year', s.birthday) AS "Sünniaasta",
	SUM(l.amount) AS "Laenusummad kokku"
FROM
	student AS s
JOIN
	loan AS l
	ON s.id = l.student_id
JOIN
	loan_type AS lt
	ON l.loan_type_id = lt.id
GROUP BY
	s.birthday
-- Having on nagu where, aga peale GROUP BY kasutamist
-- filtreerimisel saab kasutada ainult neid välju, mis on
-- group by-s ja agregaatfunktsioone
HAVING
	date_part('year', s.birthday) IS NOT NULL
	AND SUM (l.amount) > 500
	-- saaks ka lisada nt, et anna ainult need read, kus laene kokku 2 AND COUNT (l.amount) = 2
ORDER BY
date_part('year', s.birthday) ASC

-- mis aastal sündinud võtsid suurima summa laenu?

SELECT
	date_part('year', s.birthday) AS "Sünniaasta",
	SUM(l.amount) AS "Laenusummad kokku"
FROM
	student AS s
JOIN
	loan AS l
	ON s.id = l.student_id
GROUP BY
	date_part('year', s.birthday),
	l.amount
ORDER BY
	l.amount DESC
	LIMIT 1
	
-- name, count, sum

SELECT
lt.name, COUNT(l.amount), SUM (COALESCE(l.amount, 0))
FROM
loan_type AS lt
LEFT JOIN
loan AS l
ON l.loan_type_id = lt.id
GROUP BY
lt.name

-- anna õpilaste eesnime esinemise statistika ehk siis mitme õpilase eesnimi algab mingi tähega
-- ehk siis mitme õpilase eesnimi algab mingi tähega ala m 2, s 3

SELECT
SUBSTRING(s.first_name, 1, 1) AS "Eesnime esitäht",
COUNT (SUBSTRING(s.first_name, 1, 1)) AS "Mitu korda täht esineb"
FROM
student AS s
GROUP BY
s.first_name
ORDER BY
COUNT (SUBSTRING(s.first_name, 1, 1)) DESC

-- string functions: http://www.postgresqltutorial.com/postgresql-string-functions/

-- teise sõna välja selekteerimine
SELECT
SUBSTRING('lauri mattus',
		  POSITION(' ' in 'lauri mattus') +1,
				  1000)
				  
				  -- saab ka nii:
SUBSTRING('lauri mattus', 6) -- ehk siis alates 6ndast kohast, siis annab "mattus"
				  
				  
-- subquery / innter query / nester query - sünonüümid
-- ehk üks select teise selecti sees

SELECT
first_name, last_name
FROM
student

WHERE
height = (SELECT ROUND(AVG(height)) FROM student)

-- kui on kaks vastet või rohkem, pane "IN", kui üks vaste, võib panna =

-- anna õpilased, kus eesnimi on keskmise pikkusega õpilaste kesmine nimi

SELECT
first_name, last_name
FROM
student
WHERE
first_name IN (
SELECT middle_name FROM student WHERE height = (SELECT ROUND(AVG(height)) FROM student))

-- ühest tabelist teise tõstmine (copymine)
-- ütled, kuhu tahad, et need lähevad, select peab siis sama olema
INSERT INTO employee (first_name, last_name, birthday)
SELECT
first_name, last_name, birthday
FROM
student
ORDER BY
birthday
LIMIT 2

-- üks mitmele ja mitu mitmele seos
-- mitu mitmele seose puhul on vaja seosetabeleid
-- seosetabelis näitad seoses, saab panna ka seosete kuupäevi, aegu

-- anna mulle kõik matemaatikat õppivad üliõpilased

SELECT
	s.name, st.first_name, st.last_name
FROM
	subject AS s
JOIN
	student_subject AS ss
	ON ss.subject_id = s.id
JOIN
	student AS st
	ON st.id = ss.student_id
GROUP BY
	s.name,
	st.first_name,
	st.last_name
HAVING
	s.name IN ('matemaatika')

-- anna mulle kõik õppeained, kus mari õpib

SELECT
	st.first_name, st.last_name, s.name
FROM
	subject AS s
JOIN
	student_subject AS ss
	ON ss.subject_id = s.id
JOIN
	student AS st
	ON st.id = ss.student_id
GROUP BY
	s.name,
	st.first_name,
	st.last_name
HAVING
	st.first_name IN ('Mari')
	
	
	----
	-- MySQL-is on database ' Schema '