package com.valiit;

public class Main {

        static int a = 3;

        public static void main(String[] args) {
            int b = 7;
            System.out.println(b + a);

            a = 0;

            System.out.println(b + a);
            System.out.println(increasByA(10));

            // Shadowing, ehk siis sees pool defineeritud muutuja
            // varjutab väljaspool oleva muutuja
            int a = 6;

            System.out.println(b + a);


            // Nii saan klassi muutuja a väärtust muuta
            Main.a = 8;
            // Meetodis defineeritud a väärtuse muutmine
            a = 5;
            System.out.println(increasByA(10));

            System.out.println();
            System.out.println(b + Main.a);
            System.out.println(b + a);
        }

        static int increasByA(int b) {

            return b += a;
        }
    }

