package com.valiit;

public class Main {

    public static void main(String[] args) {
        // deklareerime/defineerime tekstitüüpi muutuja (variable)
        // mille nimeks paneme name ja väärtuseks Kea
	    String name = "Kea";
	    String perekonnanimi = "Kruuse";
	    System.out.println("Hello " + name + " " + perekonnanimi);
    }
}
