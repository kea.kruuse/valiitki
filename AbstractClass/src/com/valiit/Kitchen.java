package com.valiit;

public abstract class Kitchen { // Võrreldes tavalise klassiga tuleb lihtsalt üks sõna juurde: "abstract"

    private double height;
    private double width;
    private double length;

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public double getLength() {
        return length;
    }

    public void setLength(double length) {
        this.length = length;
    }

    // Samas saab teha ka selliseid meetodeid nagu interface'il.

    public abstract void becomeDirty(); // abstract meetodil ei saa sisu olla. Toimib nagu interface.
}
