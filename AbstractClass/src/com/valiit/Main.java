package com.valiit;

public class Main {

    public static void main(String[] args) {
	    // Abstract klass on hübriid liidesest ehk interface'ist ja klassist
        // Selles klassis saab defineerida nii meetodite struktuure (nagu liides), aga
        // saab ka defineerida meetodi koos sisuga
        // Abstract klassist ei saa otse objekti luua, saab ainult pärineda
        // Sama lugu tegelikult ka interface'iga.

        ApartementKitchen kitchen = new ApartementKitchen();
        kitchen.setHeight(222);
        kitchen.becomeDirty();

        //toString() annab võimaluse välja printida.
//        The method is used to get a String object representing the value of the Number Object.
//        If the method takes a primitive data type as an argument, then the String object
//        representing the primitive data type value is returned.
//         If the method takes two arguments, then a String representation of the first argument
//         in the radix specified by the second argument will be returned.
//          Seda kutsutakse välja siis, kui 1) midagi prindin, ala println("Tere" + 2) 2) liidan stringe

        // Vt ülevalt code, override method

    }
}
