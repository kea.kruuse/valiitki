package com.valiit;

import java.util.ArrayList;
import java.util.List;

public class Language {
    private String languageName;
    private List<String> countries = new ArrayList<>();


    public String getLanguageName() {
        return languageName;
    }

    public void setLanguageName(String languageName) {
        this.languageName = languageName;
    }


    public List<String> getCountries() {
        return countries;
    }

    public void setCountries(String country) {
        countries.add(country);
    }


    @Override
    public String toString() {
        String listOfCountries = "";

        for (int i = 0; i < countries.size(); i++) {
            if (i == countries.size() -1) {
                listOfCountries = listOfCountries + countries.get(i);
            }
        else {
                listOfCountries = listOfCountries + countries.get(i) + ", ";
            }
        }

        return listOfCountries;
    }
}
