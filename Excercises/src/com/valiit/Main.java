package com.valiit;

import java.util.Date;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Scanner;

import static java.lang.Integer.parseInt;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        // 1. Arvuta ringi pindala, kui teada on raadius
        // Prindi pindala välja erkaanile

        int radius;
        double area;
        boolean number;

        do {
            try {
                System.out.print("Sisesta ringi raadius:");
                radius = Integer.parseInt(scanner.nextLine());
                area = Math.PI * radius * radius;
                System.out.printf("Ringi pindala on : %.2f.%n", area);
                System.out.println();
                number = true;
            } catch (NumberFormatException e) {
                System.out.println("Tegu peab olema numbriga.");
                number = false;
            }
        } while (!number);

        //2. ülesande lahendus on all.

        areStringsEqual("tere", "tere");

        // 3. ülesande lahendus all

        int[] integers = new int[]{2, 3, 5};

        integersToString(integers);

        System.out.println();

        // 4. ülesande meetod all

        boolean correct;
        do {
            try {
                correct = true;
                System.out.println("Sisesta aasta.");
                int inputYear = Integer.parseInt(scanner.nextLine());
                correct = true;
                System.out.println();

                if (inputYear < 500 || inputYear > 2019){
                    System.out.println("Sisestatud aasta ei tohi olla väiksem kui 500 ega suurem kui 2019.");
                    correct = false;
                }
                else {
                    allLeapYearsInCentury(inputYear);
                }

            } catch (NumberFormatException e) {

                System.out.println("Ebasobiv sisend. Peab olema number."); // Miks ta esimene kord siia lööb?
                System.out.println();
                e.printStackTrace();
                correct = false;

            }
        } while (!correct);

        System.out.println();

        // 5. Defineeri klass Language, sellel klassil getLanguageName, setLanguageName ning list riikide nimedega
        // kus seda keelt räägitakse. Kirjuta üle selle klassi meetod toString() nii, et see tagastab
        // riikide nimekirja eraldades komaga.

        // Tekita antud klassist 1 objekt ühe vabalt valitud keele andmetega ning prindi välja
        // selle objekti toString() meetodi sisu.
        // Mõtle, mida on mitu ja mida on üks. Üks keel ja tema küljes mitu riiki mitte vastupidi.

        Language argaad = new Language();
        argaad.setLanguageName("Argaad");

        argaad.setCountries("Hardo");
        argaad.setCountries("Tšiili");
        argaad.setCountries("Brasiilia");
        argaad.setCountries("Kreeka");

        System.out.println(argaad.toString());

    }

    // 2. Kirjuta meetod, mis tagastab boolean tüüpi väärtuse
    // ja mille sisendparameetriteks on kaks stringi.
    // Meetod tagastab kas tõene või vale selle kohta,
    // kas stringid on võrdsed

    static boolean areStringsEqual(String one, String two) {

        return one.equals(two);

//        if (one.equals(two)){
//            System.out.println("Tekstid üks ja kaks on identsed.");
//            System.out.println();
//            return true;
//        }
//        else {
//            System.out.println("Tekstid üks ja kaks ei ole identsed.");
//            System.out.println();
//            return false;
//        }
    }

    // 3. Kirjuta meetod, mille sisendparameetriks on täisarvude
    // massiiv ja mis tagastab stringide massivi.
    // Iga sisend-massiivi elemendi kohta olgu tagastatavas massiivis samapalju a tähti.
    // 3, 6, 7
    // "aaa", "aaaaaaa", "aaaaaaa"

    public static String[] integersToString(int[] integers){
        String aas = "";
        String[] newStrings = new String[integers.length];

        for (int i = 0; i < integers.length; i++) {
            aas = "";

                for (int j = 0; j < Integer.valueOf(integers[i]); j++) {
                    aas = aas + "a";
                }

                newStrings[i] = aas;
                System.out.println(newStrings[i]);

            // alternatiiv
            // for-i sisse: newString[i] = integers[i]*a;
            //
        }

        return newStrings;
    }


   // 4. Kirjuta meetod, mis võtab sisendparameetrina aastaarvu
    // ja tagastab kõik sellel sajandil esinenud liigaastad
    // Sisestada saab ainult aastaid vahemikus 500-2019.
    // Ütle veateade, kui aastaarv ei mahu vahemikku.

    public static void allLeapYearsInCentury(int year) {

        int centuryStart = year / 100 * 100;
        int centuryEnd = centuryStart + 99;

        int leapyear;

        int centuryYear;
        centuryYear = centuryStart;

        // Variant on teha ka ArrayListina

        List<Integer> leapYears = new ArrayList<>();

        for (int i = centuryStart; i < centuryEnd; i++) {
            centuryYear++;

            if (centuryYear % 4 == 0) {
                leapyear = centuryYear;
                System.out.println("Liigaasta on: " + leapyear);
                leapYears.add(leapyear);
            }

        }
        // võimalik teha ka nii, et tagastab return leapYears; Sel juhul peaks main-is välja printima for-eachiga.

    }
//
//    static int centuryFromYear(int year) {
//        if (year % 100 == 0) {
//            year = year / 100;
//        } else {
//            year = (year / 100) + 1;
//        }
//        return year;
    }

