package com.valiit;

public class Main {

    public static void main(String[] args) {
        Monitor firstMonitor = new Monitor();
        Monitor secondMonitor = new Monitor();
        Monitor thirdMonitor = new Monitor();

        firstMonitor.manufacturer = "Philips";
        firstMonitor.color = Color.WHITE;
        firstMonitor.diagonal = 27;
        firstMonitor.screenType = ScreenType.LCD;

        secondMonitor.manufacturer = "LG";
        secondMonitor.color = Color.BLACK;
        secondMonitor.screenType = ScreenType.TFT;
        secondMonitor.diagonal = 30;

        thirdMonitor.manufacturer = "PC";
        thirdMonitor.color = Color.BLUE;
        thirdMonitor.screenType = ScreenType.LCD;
        thirdMonitor.diagonal = 21;


        System.out.println(firstMonitor.manufacturer);
        System.out.println(secondMonitor.screenType);

        firstMonitor.color = Color.BLACK;

        System.out.println(firstMonitor.color);

        Monitor[] monitors = new Monitor[3];

        monitors [0] = firstMonitor;
        monitors [1] = secondMonitor;
        monitors [2] = thirdMonitor;

        // Prindi välja kõigi monitoride tootja, mille diagonaal on suurem kui 25 tolli

        System.out.println();

        for (int i = 0; i < monitors.length; i++) {
            if(monitors[i].diagonal > 25){
                System.out.println("Selle tootja monitori diagonaal on suurem kui 25 tolli: " + monitors[i].manufacturer);
            }
        }

        //Leia monitori värv kõige suuremal monitoril

        Monitor maxSizeMonitor = monitors [0];

        for (int i = 0; i < monitors.length; i++) {
            if (monitors[i].diagonal > maxSizeMonitor.diagonal){
                maxSizeMonitor = monitors[i];
            }
        }
            System.out.println();
            maxSizeMonitor.printInfo();

        try {
            System.out.printf("Monitori diagonaal cm-tes on %.2f%n", maxSizeMonitor.diagonalinCentimeters());
        } catch (Exception e) {
            System.out.println("Ei tööta.");;
        }
    }
}
