package com.valiit;

// num on tüüp, kus saab defineerida erinevaid lõplikke valikuid
// Tegelikult salvestatakse enum alati int-na. Javas see veidi keerulisem.
// Kui soovime enumis kasutada mingeid teisi numbreid: vt http://tutorials.jenkov.com/java/enums.html

enum Color {
    BLACK,
    WHITE,
    GRAY,
    BLUE,
    GREEN,
    YELLOW,
    RED
}

enum ScreenType {
    LCD,
    TFT,
    OLED,
    AMOLED
}

public class Monitor {
    String manufacturer;
    double diagonal; // tollides
    Color color;
    ScreenType screenType;

    void printInfo() {

        System.out.println("Monitori info:");
        System.out.printf("Tootja: %s%n", manufacturer);
        System.out.printf("Diagonaal: %.1f%n", diagonal); // ümardab ühe kohani
        System.out.printf("Värv: %s%n", color);
        System.out.printf("Ekraani tüüp: %s%n", screenType);
        System.out.println();
    }

    // Meetod mis tagastab ekraani diagonaali inches sentimeetiteks

    double diagonalinCentimeters() {
            return diagonal * 2.54f;
    }
}
