package com.valiit;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

	    System.out.print("Sisesta number");
	    int a = Integer.parseInt(scanner.nextLine());

	    // Kaks võrdusmärki, mis kontrollib, kas on võrdne. Üks võrdusmärk annaks väärtuse.
        // Kas väärtuse andmine (=) või võrdlemine (==).
        // === tähendaks, et lisaks väärtusele võrdled ka tüüpi.
        //Java on type-safe language ehk tüübikindel. Igal muutujal alati tüüp.
	    if(a == 3){
	        System.out.printf("Arv %d on võrdne kolmega.%n", a);
        }
	    if(a<5){
	        System.out.printf("Arv %d on väiksem viiest.%n", a);
        }
	    if(a>2){
	        System.out.printf("Arv %d on suurem kahest.%n", a);
        }
	    // Hüüumärk tähistab eitust.
        if(a != 4){
            System.out.printf("Arv %d ei võrdu neljaga.%n", a);
        }
        if(a>=7){
            System.out.printf("Arv %d on suurem või võrdne seitsmega.%n", a);
        }
        // Hüüumärk sulgude ees tähendab, et juhul kui sulgude sees olev asi on vale.
        if(!(a>=7)){
            System.out.printf("Arv %d ei ole suurem või võrdne seitsmega.%n", a);
        }
        // Kui tingimuste vahel on &&, kui üks tingimustest leitakse, et on vale
        // siis edasisi tingimusi enam ei vaadata.
        if(a > 2 && a < 8){
            System.out.printf("Arv %d on suurem kahest ja väiksem kaheksast.%n", a);
        }
        //arv on väiksem kui 2 või arv on suurem kui 8
        if(a < 2 || a > 8){
            System.out.printf("Arv %d on väiksem kui kaks või arv on suurem kui kaheksa.%n", a);
        }
        //kui arv on 2 ja 8 vahel või arv on 5 ja 8 vahel või arv on suurem kui 10
        if((a > 2 && a < 8) || ( a > 5 && a < 8) || a>10){
            System.out.printf("Arv %d on 2 ja 8 vahel või arv on 5 ja 8 vahel või arv on suurem kui 10.%n", a);
        }
        // kui arv ei ole 4 ja 6 vahel aga on 5 ja 8 vahel
        // või arv on negatiivne aga pole suurem kui 14
        // jaatus on prioriteetsem kui või
        if(!(a>4 && a<6) && (a<5 && a<8) || (a < 0) && !(a>-14)){
            System.out.printf("Arv %d ei ole 4 ja 6 vahel aga on 5 ja 8 vahel või arv on negatiivne, aga pole suurem kui 14.%n", a);
        }

    }
}
