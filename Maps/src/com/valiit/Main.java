package com.valiit;

import java.util.HashMap;
import java.util.Map;

public class Main {

    public static void main(String[] args) {
	    Map<String, String> map = new HashMap<String, String>();

	    // Sõnaraamat
        // Maja -> House
        // Isa -> Father
        // Hoiad väärtuste paare

        // key ja value ehk võti ja väärtus

        // paljudes programmeerimiskeeltes nimetataksegi map-i dictionary'ks
        // saad hoida paare, sh ka nt string ja objekt või int ja string jne
        // seda võib võtta ala nagu igale väärtusele vastab indeksi asemel key, mis võib olla ka nt string
        // töövahend, mis võimaldab kiiresti otsida.
        // andmed on omavahel mapitud, ühest teise teisendus
        // seda kasutatakse ka rakendustes tõlkimiseks.

        map.put("Maja", "House");
        map.put("Isa", "Father");
        map.put("Sinine", "Blue");
        map.put("Puu", "Tree");

// algoritm map-i printimiseks:
        for (Map.Entry<String, String> entry :map.entrySet()) {
            System.out.printf(entry.getKey() + " " + entry.getValue() + "%n");
        }

        // NB map-il pole järjekorda! Map-i puhul küsid key järgi või value järgi. Map järjekorda ei salvesta.

        System.out.println();


        // Oletame, et tahan teada, mis on inglise keeles puu

        String translation = map.get("Puu");

        System.out.println(translation);
        System.out.println();

        Map<String, Long> nameAndID = new HashMap<String, Long>();

        nameAndID.put("Kea", 49505120212L);

        Long idCode = nameAndID.get("Kea");

        System.out.println(idCode);
        System.out.println();

        // Kui kasutada sama put key lisamisel, kirjutatakse value üle.

        map.remove("Sinine");

        // Loe lauses üle kõik erinevad tähed ja prindi välja iga tähe järel, mitu tükki teda selles lauses oli

        // e 2
        // l 1
        // a 2 jne

         // char suudab hoida phte sümbolid, pannakse "" vahel, saab teha massiivi jne



        // mingite asjade kokkulugemise algoritm:

        String sentence = "elas metsas mutionu";
        Map<Character, Integer> letterCounts = new HashMap<Character, Integer>();

        char[] characters = sentence.toCharArray();

        for (int i = 0; i < characters.length; i++) {
            if(letterCounts.containsKey(characters[i])) {

                letterCounts.put(characters[i], letterCounts.get(characters[i]) + 1); // kui olemas, lisan ühe juurde.
            }else {
                letterCounts.put(characters[i], 1); // kui esimese leian, saan panna, et teda on 1
            }
        }

        // Map.Entry<Character, Integer> on klass, mis hoiab endas ühte rida map-is
        // ehk ühte key-value paari

        System.out.println(letterCounts);

        System.out.println();

        for (Map.Entry<Character, Integer> entry : letterCounts.entrySet()) { // entry tähistab ühte rida mapi paarist. Iga korduse ajal saab mõlemat küsida.
            // et seda entryt kätte saada, on eraldi meetod entrySet() . for-each map.
            System.out.printf("Tähte %s esines %d korda%n", entry.getKey(), entry.getValue()); // tal on kaks meetodit, üks tagastab key ja teine value
        }


        // e 2
        // l 1
        // a 2
        // s 3


        map.keySet(); // saab küsida kõiki key-sid


        // Map on interface
        // HashMap on class, mis implemendib map-i
        // LinkedHashMap on class, mis pärineb HashMapist

        // LinkedHashMap provides a special constructor which enables us to specify, among custom load factor (LF)
        // and initial capacity, a different ordering mechanism/strategy called access-order:

        // Kui tahame, et säiliks lisamise järjekord, kasutame LinkedHashMap-i

        // LinkedHashMap<Integer, String> map = new LinkedHashMap<>(16, .75f, true);
        // The first parameter is the initial capacity, followed by the load factor
        // and the last param is the ordering mode. So, by passing in true, we turned out access-order, whereas the default was insertion-order.

        // Kollektsioone on Java-s palju.
        // https://en.wikiversity.org/wiki/Java_Collections_Overview


    }
}
