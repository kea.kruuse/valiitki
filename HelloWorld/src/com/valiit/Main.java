package com.valiit;
// public tähendab, et klass, meetod või muutuja on avalikult nähtav/ligipääsetav
// class - javas üksus, üldiselt on ka eraldi fail, mis sisaldab/grupeerib mingit funktsionaalsust
// HelloWorld - klassi nimi, mis tavaliselt on ka faili nimi (faili nimi peab olema sama, muidu ei tööta)
// static - meetodi ees tähendab, et seda meetodit saab välja kutsuda ilma klassist objekti loomata st saab välja kutsuda nt HelloWorld.main Kui static sõna pole, tuleb luua objekt ja siis välja kutsuda.
// void - tähendab seda, et meetod ei tagasta midagi. Meetodile on võimalik anda kaasa parameetrid, mis pannakse sulgude sisse eraldades komaga.
// Stringi [] - tähistab stringi massiivi
// args on selle massiivi nimi. sisaldab käsurealt kaasa pandud parameetreid.
// System.out.println - on java meetod, millega saab printida välja rida teksti.
// see, mis kirjutatakse sulgudesse, prinditakse välja ja tehakse reavahetus.
public class Main {

    public static void main(String[] args) {

        System.out.println("Hello World!");
    }
}
