package com.valiit;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {
	// Generic ArrayList on selline, kus objekti loomise hetkel peame määrama, mis tüüpi elemente see sisaldama
    // hakkab.

        List<Integer> numbers = new ArrayList<>();
        // võrdluseks, et mitte-generic ehk tavaline ArrayList on List list = new ArrayList();

       // nüüd ala nii ei saa: numbers.add("tere"); sest listi võib panna ainult int-id

        numbers.add(10);
        numbers.add(13);
        numbers.add(30);
        numbers.add(-77);
        numbers.add(Integer.valueOf("11"));

        List<String> words = new ArrayList<>();
        words.add("Tere");
        words.add(" tulemast");
    }
}
