package com.valiit;

public class Main {

    public static void main(String[] args) {
	float a = 123.34f;
	double b = 45546.34556;

	System.out.println("Arvude " + a + " ja " + b + " korrutis on " + (a*b) +" ja jagatis on " + (a/b) + ".");
	System.out.printf("Arvude %g ja %g korrutis on %g ja jagatis on %g.%n",
			a, b, a*b, a/b);

	float o = 130.345567853557768785f;
	float p = 9;
	double v = 130.345567853557768785;
	double z = 9.;
		System.out.printf("Arvude %g ja %g korrutis on %g ja jagatis on %g.%n",
				o, p, o*p, o/p);
		System.out.printf("Arvude %g ja %g korrutis on %g ja jagatis on %g.%n",
				v, z, v*z, v/z);
		// Double'l on kaks korda rohkem komakohti kui floatil.
		// Double on täpsem.

    }
}
