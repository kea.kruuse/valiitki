package com.valiit;

import java.io.*;
import java.nio.charset.Charset;
import java.sql.SQLOutput;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

/*        Vali toiming
        a) sularaha sissemakse
        b) sularaha väljamakse
        c) Kontojääk
        d) Lõpetamine
        e) Muuda pinkoodi

        Valiti b
        Vali summa:
        a) 5 eur
        b) 10 eur
        c) 20 eur
        d) Muu summa

        Valiti d:
        Sisesta pinkood
        Sisesta uus pinkood

        */
        // lisa kontoväljavõtte funktsionaalsus

        Scanner scanner = new Scanner(System.in);

/*        for (int i = 0; i < 3; i++) {
            System.out.println("Sisesta pinkood");
            enteredPin = scanner.nextLine();
            System.out.println();

            if(enteredPin.equals(pin)) {
                System.out.println("Tore! Õige pinkood.");
                System.out.println();
                break;
            }*/
        String transaction;
        double balance = loadBalance();
        String pin = loadPin();
        String enteredPin;

        //validatePin(); // kutsub välja pin koodi küsimise, aga see iflause juba teeb seda.
        if(!validatePin()){
            System.out.println("Kaart konfiskeeritud.");
            return; // return lihtsalt lõpetab töö ära. Viskab main meetodist välja.
        }

        System.out.println();

        boolean wrongAnswer;
        do {
            do {

                System.out.println("Vali toiming:");
                System.out.println("a) Sularaha sissemakse");
                System.out.println("b) Sularaha väljamakse");
                System.out.println("c) Kontojääk");
                System.out.println("d) Lõpetamine");
                System.out.println("e) Paroolivahetus");
                System.out.println("f) Prindi kontoväljavõte");

                String answer = scanner.nextLine();
                wrongAnswer = false;
                switch (answer) {
                    case "A":
                    case "a":
                        System.out.printf("Sisesta sularaha.%n");
                        int cashIn = Integer.parseInt(scanner.nextLine());
                        saveBalance((int) (balance + cashIn));
                        System.out.printf("Sularaha sisestatud. Sinu kontojääk on nüüd %g.%n", loadBalance());
                        transaction = String.format("Sularaha sissemakse %d EUR.", cashIn);
                        saveTransaction(currentDateTimeToString() + " " + transaction);
                        System.out.println();
                        break;
                    case "B":
                    case "b":
                        System.out.printf("Sisesta soovitud summa.%n" +
                                "a) 5 EUR %n" +
                                "b) 10 EUR %n" +
                                "c) Muu summa%n");
                        String cashOutAnswer = scanner.nextLine();
                        System.out.println();
                        switch (cashOutAnswer){
                            case "a":
                            case "A":
                                validatePin();
                                if(loadBalance() > 5){
                                    balance -= 5;
                                    saveBalance((int) (balance));
                                    System.out.printf("Palun, siin on 5 EUR.%n"+
                                            "Teie kontojääk on %g.%n", loadBalance());
                                    transaction = "Sularaha väljamakse 5 EUR.";
                                    saveTransaction(currentDateTimeToString() + transaction); // võib panna ka siia sisse otse transactioni teksti
                                    // alternatiiv: fileWriter.append(String.format(%s Raha väljamakse -%d%n" + currentDateTimeToString + amount); amount tuleks siis defineerida
                                    System.out.println();
                                }
                                if (loadBalance() < 5){
                                    System.out.println("Kontol pole piisavalt vahendeid.");
                                    System.out.println();
                                }
                                break;
                            case "b":
                            case "B":
                                validatePin();
                                if(loadBalance() > 10){
                                    balance -= 10;
                                    saveBalance((int) (balance));
                                    System.out.printf("Palun, siin on 10 EUR.%n"+
                                            "Teie kontojääk on %g.%n", loadBalance());
                                    transaction = "Sularaha väljamakse 10 EUR.";
                                    saveTransaction(currentDateTimeToString() + " " + transaction);
                                    System.out.println();
                                }
                                if (loadBalance() < 10){
                                    System.out.println("Kontol pole piisavalt vahendeid.");
                                    System.out.println();
                                }
                                break;
                            case "c":
                            case "C":
                                validatePin();
                                System.out.println("Palun sisestage väljavõetav summa.");
                                int cashOut = Integer.parseInt(scanner.nextLine());
                                if(loadBalance() > cashOut){
                                    balance -= cashOut;
                                    saveBalance((int) (balance));
                                    System.out.printf("Palun, siin on %d EUR.%n"+
                                            "Teie kontojääk on %g.%n", cashOut, loadBalance());
                                    transaction = String.format("Sularaha väljamakse %d EUR.", cashOut);
                                    saveTransaction(currentDateTimeToString() + " " + transaction);
                                    System.out.println();
                                }
                                if (loadBalance() < cashOut){
                                    System.out.println("Kontol pole piisavalt vahendeid.");
                                    System.out.println();
                                }
                                break;
                        }
                        break;
                    case "C":
                    case "c":
                        System.out.printf("Kontojääk on %g.", loadBalance());
                        break;
                    case "D":
                    case "d":
                        System.out.println("Head aega :)");
                        break;
                    case "E":
                    case "e":
                        validatePin();
                        String newPin = null;
                        try {
                            System.out.println("Sisesta uus pinkood");
                            newPin = scanner.nextLine();
                        } catch (Exception e) {
                            System.out.println("Pinkood ebasobilik.");
                        }
                        savePin(newPin);
                        System.out.println("Uus pinkood on salvestatud.");
                        System.out.println();
                        break;
                    case "F":
                    case "f":
                        printTransactions();
                        System.out.println();
                        break;
                    default:
                        System.out.println("Selline tehing puudub");
                        wrongAnswer = true;
                        break;
                }

            } while (wrongAnswer);

            System.out.println("Kas tahad jätkata? j/e");
        } while (scanner.nextLine().equals("j"));
    }

    static void savePin(String pin){
        // Toimub pin kirjutamine txt faili
        try {
            FileWriter fileWriter = new FileWriter("pin.txt", Charset.forName("Cp1252"));
            fileWriter.write(pin + System.lineSeparator());
            fileWriter.close();
        } catch (IOException e) {
            System.out.println("Pinkoodi salvestamine ebaõnnestus.");
        }
    }
    // toimub pin välja lugemine pin.txt failist
    // toimub shadowing
    // Shadowing refers to the practice in Java programming of using two variables with the same name within scopes that overlap.
    // When you do that, the variable with the higher-level scope is hidden because the variable with lower-level scope overrides it.
    // The higher-level variable is then “shadowed.”

    static String loadPin() {
        // Toimub pin väljalugemine pin.txt failist
        String pin = null; // võib ka "" panna
        try {
            FileReader fileReader = new FileReader("pin.txt", Charset.forName("Cp1252"));
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            pin = bufferedReader.readLine();
            fileReader.close();
            bufferedReader.close();
        } catch (IOException e) {
            System.out.println("Pin koodi lugemine ebaõnnestus");
        }
        return pin;
    }

    static void  saveBalance(int balance) {
        // Toimub balance kirjutamine balance.txt faili
        try {
            FileWriter fileWriter = new FileWriter("balance.txt", Charset.forName("Cp1252"));
            fileWriter.write(balance + System.lineSeparator());
            fileWriter.close();
        } catch (IOException e) {
            System.out.println("Kontoseisu uuendandamine ebaõnnestus.");
        }
    }

    static double loadBalance() {
        // Toimub balance väljalugemine balance.txt failist
        double balance = 0;
        try {
            FileReader fileReader = new FileReader("balance.txt", Charset.forName("Cp1252"));
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            balance = Integer.parseInt(bufferedReader.readLine());
            fileReader.close();
            bufferedReader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return balance;
    }


    static String currentDateTimeToString () {
        DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
        Date date = new Date();
        // Get current date
        return dateFormat.format(date);
    }

    static void saveTransaction(String transaction) {
        try {
            FileWriter fileWriter = new FileWriter("kontovaljavote.txt", Charset.forName("Cp1252"), true);
            fileWriter.append(transaction + System.lineSeparator());
            fileWriter.close();
        } catch (IOException e) {
            System.out.println("Kontoväljavõtte uuendandamine ebaõnnestus.");
        }
    }


    static boolean validatePin() {
        Scanner scanner = new Scanner(System.in);
        for (int i = 0; i < 3; i++) {
            System.out.println("Palun sisesta pinkood");

            String enteredPin = scanner.nextLine();

            if(enteredPin.equals(loadPin())) {
                System.out.println("Õige pin!");
                return true;
            }
        }
        return false;
    }

    static void printTransactions() {
        try {
            System.out.println("Konto väljavõte:");
            FileReader fileReader = new FileReader("kontovaljavote.txt", Charset.forName("Cp1252"));

            BufferedReader bufferedReader = new BufferedReader(fileReader);

            // readLine loeb iga kord välja kutsudes järgmise rea
            String line = bufferedReader.readLine();

            // Kui readLine avastab, et järgmist rida tegelikult ei ole,
            // siis tahastab see meetod null
            while (line != null) {
                System.out.println(line);
                line = bufferedReader.readLine();
            }

            bufferedReader.close();
            fileReader.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}