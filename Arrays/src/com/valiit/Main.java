package com.valiit;

public class Main {

    public static void main(String[] args) {
	// Järjend, massiiv, nimekiri
    // saad ühes muutujas hoida sama tüüpi elemente
    // lihtsaim neist täisarvumassiiv
        //luuakse täisarvude massiiv, millesse mahub 5 elementi.
        // loomise hetkel määratud elementide arvu hiljem muuta ei saa.
        int[] numbers = new int[5];

        // indeksid algavad nullist.
        // viimane indeks on alati ühe võrra väiksem kui massiivi pikkus.
        numbers[0] = 2;
        numbers[1] = 7;
        numbers[2] = -2;
        numbers[3] = 11;
        numbers[4] = 1;

        // nii prindid välja soovitava numbri indeksi kaudu
        System.out.println(numbers[3]);

        System.out.println();

        // nii saab printida for tsükli abiga kõik välja
        // massiivi liikmete arvu saab panna numbers-length
        // st kui pärast muudad elementide arvu, toimivad ikka
        for (int i = 0; i < numbers.length; i++) {
            System.out.println(numbers[i]);
        }

        System.out.println();
        // prindi numbrid tagurpidises järjekorras
        for (int i = 4; i >= 0; i--) {
            System.out.println(numbers[i]);
        }
        // prindi numbrid, mis on suuremad kui 2
        for (int i = 4; i >= 0; i--) {
            if(numbers[i] > 2){
                System.out.println(numbers[i]);
            }
        }
        System.out.println();
        // prindi kõik paarisarvud
        for (int i = 4; i >= 0; i--) {
            if(numbers[i] % 2 == 0) {
                System.out.println(numbers[i]);
            }
        }
        System.out.println();
        // prindi tagant poolt 2 esimest paaritut arvu
        int count = 2;
        for (int i = 4; i >= 0; i--) {
            if(numbers[i] % 2 != 0){
                System.out.println(numbers[i]);
                count--;
            }
            if(count == 0){
                break;
            }
        }
        System.out.println();
        // loo teine massiiv kolmele numbrile ja pane sinna esimesest massiivist
        // kolm esimest numbrid
        // ja prindi teise massiivi elemendid ekraanile

        int[] secondNumbers = new int[3];

        for (int i = 0; i < secondNumbers.length; i++) {
            secondNumbers[i]= numbers[i];
            System.out.println(secondNumbers[i]);
        }

        System.out.println();
        // loo kolmas massiiv ja võta sinna esimesest massiivist kolm numbrit
        // tagantpoolt alates
        // secondNumbers[0] = numbers[4];
        // secondNumbers[1] = numbers[3];
        // secondNumbers[2] = numbers[2];

        int[] thirdNumbers = new int[3];

        for (int i = 0; i < thirdNumbers.length; i++) {
            thirdNumbers[i]= numbers[numbers.length -i -1];
            System.out.println(thirdNumbers[i]);
        }

        System.out.println();

        // i = i + 2 => i += 2
        // i = i -4 => i -= 4
        // i = i * 3 => i *= 3 arvu iseenda korrutamine mingi numbriga
        // i = i / 2 => i /= 2

        // Alternatiiv eelnevale.

        for (int i = 0, j = 4; i < thirdNumbers.length; i++, j--) {
            thirdNumbers[i]= numbers[j];
            System.out.println(thirdNumbers[i]);
        }

        // Nt võita iga kümnes number, saaks panna j-=10
        // Tahad üle ühe, siis nt j-=2
    }
}
