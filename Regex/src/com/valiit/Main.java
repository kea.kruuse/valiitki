package com.valiit;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Main {

    public static void main(String[] args) {
        System.out.println("Sisesta e-mail");
        Scanner scanner = new Scanner(System.in);
        String email = scanner.nextLine();
        String regex = "([a-z0-9\\._]{1,50})@[a-z0-9]{1,50}\\.([a-z]{2,10})\\.?([a-z]{2,10})?";

        if (email.matches(regex)) {
            System.out.println("Email on korrentne");
        } else {
            System.out.println("Email ei ole korrektne");
        }
        Matcher matcher = Pattern.compile(regex).matcher(email);
        if(matcher.matches()){
            System.out.println("Kogu e-mail: " + matcher.group(0));
            System.out.println("Tekst vasakulpool @-märki: " + matcher.group(1));
            System.out.println("Domeeni laiend: " + matcher.group(2));
        }

        // küsi kasutajalt isikukood ja valideeri, kas see on õiges formaadis
        // mõelge ise, mis piirangud isikukoodis peaks olema
        // aasta peab olema reaalne aasta, kuu number, kuupäeva number
        // ja prindi välja tema sünnipäev

        System.out.println();

        System.out.println("Sisesta isikukood");
        String isikukood = scanner.nextLine();
        regex = "([1-6])([0-9]{2})([0-1][0-9])([0-9]{2})([0-9]{4})";
        matcher = Pattern.compile(regex).matcher(isikukood);

        if (isikukood.matches(regex)) {
            System.out.println("Isikukood on korrentne");
        } else {
            System.out.println("Isikukood ei ole korrektne");
        }
        if(matcher.matches()){
            System.out.println("Kogu isikukood " + matcher.group(0));
            System.out.println("Sünni kuupäev: " + matcher.group(4) + "." + matcher.group(3) + "." + matcher.group(2));
        }

    }
}
