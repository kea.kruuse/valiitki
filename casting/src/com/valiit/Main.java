package com.valiit;

public class Main {

    public static void main(String[] args) {
	    // Casting means picking an Object of one specific type
        // and making it into another Object type.
        // This process is called casting a variable.
        // dataType variableName = (dataType) variableToConvert;

        byte a = 3;
        // Kui üks numbritüüp mahub teise sisse, siis toimub automaatne teisendamine (implicit casting)
        //
        short b = a;
        short c = 300;
        // peab veenduma, et number mahub sinna teise numbritüüpi ja kui mahub, peab ise seda teisendama.
        // selle kohta öeldakse explicit casting.
        byte d = (byte)c;

        System.out.println(d);

        long l = 10000000000L;
        int i = (int) l;
        System.out.println(i);
        // Ala teeb programmi valmis ja kui numeric data variable on vale, hakkab programm segast peksma.
        // siis peab olema kindel, et väärtused ei muutu.
        // Alati saab väiksemat numbrit suuremasse panna, aga teistpidi peab alati castima ja veenduma, et see mahub sisse.

        float q = 123.564f;
        double w = b;

        float r = (float)b;

        double s = 12E50; //12*10astmes50
        float lm = (float)s;
        System.out.println(lm);

        int yt = 3;
        double o = yt;

        // nii kaob täpsus ära
        double fr = 2.99;
        short ti = (short)fr;
        System.out.println(ti);

        // castida saab ainult numbriliste data type'idega.

        int ppr = 2;
        double ttr = 9;
        System.out.println(ppr / ttr);

        // int/int on alati int
        // int/double on alati double
        // double/int on double
        // double + int on double
        //double / float on double



        float yt = 12.34554677F;

    }
}
