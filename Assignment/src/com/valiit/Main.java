package com.valiit;

import static java.lang.Math.PI;

public class Main {

    public static void main(String[] args) {


        // 1. ülesanne

        System.out.println("Esimene ülesanne:");

        double number = PI * 2;
        System.out.println(number);

        System.out.println();

        // 2. ülesande main meetodi osa

        System.out.println("Teine ülesanne:");

        System.out.println(areIntsEqual(2, 2));

        System.out.println();
        
        // 3. ülesande main meetodi osa

        System.out.println("Kolmas ülesanne:");

        String[] words = new String[] {"Tere", "kord", "kassimajakene", "Mauriekang"};

        Integer[] numbers = stringArrayToIntArray(words);
        for (int i = 0; i < numbers.length; i++) {
            System.out.println(numbers[i]);
        }

        System.out.println();

        // 4. ülesande main meetodi osa

        System.out.println("Neljas ülesanne:");

        System.out.println(calculateCenturyFromYear(0));
        System.out.println(calculateCenturyFromYear(1));
        System.out.println(calculateCenturyFromYear(128));
        System.out.println(calculateCenturyFromYear(598));
        System.out.println(calculateCenturyFromYear(1624));
        System.out.println(calculateCenturyFromYear(1827));
        System.out.println(calculateCenturyFromYear(1996));
        System.out.println(calculateCenturyFromYear(2017));

        System.out.println();

        // 5. ülesande main meetodi osa

        System.out.println("5. ülesanne:");
        System.out.println();

        Country estporia = new Country();
        estporia.setName("Estporia");
        estporia.setPopulation(345000);
        estporia.setLanguages("Mandariini");
        estporia.setLanguages("Hartipofu");
        estporia.setLanguages("Kalagas");

        estporia.toString();

    }


    // 2. ülesanne

    static boolean areIntsEqual(Integer one, Integer two) {

        return one == two;
    }

    // 3. ülesanne


    static Integer[] stringArrayToIntArray(String[] words) {

        Integer[] numbers = new Integer[words.length];

        for (int i = 0; i < words.length; i++) {
            numbers[i] = words[i].length();

        }
        return numbers;

    }

    static byte calculateCenturyFromYear (int year) {

        int century = (year / 100) + 1;

        return (byte) century;
    }
}
