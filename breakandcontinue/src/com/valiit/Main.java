package com.valiit;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        // Küsime kasutajalt pin-koodi,
        // kui see on õige, ütleme "Tore"
        // muul juhul küsime uuesti.
        // Muul juhul küsime uuesti, aga kokku küsime kolm korda.
        // koodid ja paroolid salvestatakse tavaliselt stringidena.

        String realPin = "3456";
        String enteredPin;

        Scanner scanner = new Scanner(System.in);

        // Küsib parooli kolm korda. Kui vastus on õige, viib tsüklist välja.
        for (int i = 0; i < 3; i++) {
            System.out.println("Sisesta pinkood");
            enteredPin = scanner.nextLine();

            if(enteredPin.equals(realPin)) {
                System.out.println("Tore! Õige pinkood.");
                break;
            }
        }
//        System.out.println();
//        int triesLeft = 3;
//        do {
//            System.out.println("Sisesta pinkood");
//            enteredPin = scanner.nextLine();
//            triesLeft--;
//        }while(!realPin.equals(enteredPin) && triesLeft > 0);
//        if (triesLeft > 0) {
/*        System.out.println("Tore! Õige pinkood.");
        }
        if(triesLeft == 0){
            System.out.println("Rohkem ei saa.");
        }*/
        // Aga see olukord täpselt selline, kus oleks hea break'i kasutada.
        // pmst oleks variant kasutada ka boooleane, aga oleks samuti väga keeruline.


        // prindi välja numbrid 10-20 ja 40-60
        // continue jätav selle tsükli korduse katki ja jätkab järgmise kordusega
        for (int i = 10; i < 61; i++) {
            if(i > 20 && i < 40){
                continue;
            }
            System.out.println(i);
        }
    }
}
