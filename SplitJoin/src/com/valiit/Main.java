package com.valiit;

public class Main {

    public static void main(String[] args) {
	    String[] words = new String[] {"Põdral", "maja", "metsa", "sees"};

        for (int i = 0; i < words.length; i++) {
            System.out.println(words[i]);
        }

        System.out.println();
        // Prindi kõik sõnad massiivist, mis algavad m-tähega;
        // if tuleb juurde

        for (int i = 0; i < words.length; i++) {
            if(words[i].toLowerCase().substring(0,1).equals("m")){
                System.out.println("See sõna algab tähega m: " + words[i]);
            }
        }

        System.out.println();

        // Prindi kõik a tähega lõppevad sõnad KODUÜLESANNE
        for (int i = 0; i < words.length; i++) {
            int aIndex = words[i].toLowerCase().indexOf("a ");
            int tuhik = words[i].indexOf(" ", aIndex);
            if(words[i].toLowerCase().substring(words[i].length()-1,words[i].length()).equals("a")){
                System.out.println("See sõna lõppeb tähega a: " + words[i]);
            }
        }
        System.out.println();

        // Prindi välja kõik sõnad, kus on 4 tähte KODUTÖÖ
        for (int i = 0; i < words.length; i++) {
            if(words[i].length()==4){
                System.out.println("Selles sõnas on neli tähte: " + words[i]);
            }
        }

        // Prindi välja kõige pikem sõna KODUTÖÖ

        // Prindi välja sõna, kus on esimene ja viimane täht sama KODUTÖÖ
    }
}
