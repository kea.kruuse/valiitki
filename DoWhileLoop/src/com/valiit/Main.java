package com.valiit;

import java.util.Scanner;

public class Main {
    
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

 //       System.out.println("Kas tahad jätkata? Jah/ei");
//        String vastus = scanner.nextLine();
//
// jätkame seni, kuni kasutaja vastab ei

//        while (!vastus.toLowerCase().equals("ei")) {
//            System.out.println("Kas tahad jätkata? Jah/ei");
//            vastus = scanner.nextLine();
//        }
        // loodud muutujad kehtivad ainult {} sees.
        String vastus;

        // järelkontrolliga while tsükkel
        // do while esimene kord kordab
        do {
            System.out.println("Kas tahad jätkata? Jah/ei");
            vastus = scanner.nextLine();
        }
        while(!vastus.toLowerCase().equals("ei"));
    }
}
