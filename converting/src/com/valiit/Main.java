package com.valiit;

        import java.io.PrintStream;
        import java.util.Locale;
        import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        // Küsi kasutajalt 2 numbrit ja prindi välja nende summa.
        Scanner scanner = new Scanner( System.in );

        System.out.println("Sisesta esimene number");
        int firstNumber = Integer.parseInt(scanner.nextLine());

        System.out.println("Sisesta teine number");
        int secondNumber = Integer.parseInt(scanner.nextLine());

        System.out.printf("Summa on %d.%n",
                firstNumber+secondNumber);

        // Küsi kaks reaalarvu ja prindi nende jagatis

        System.out.println("Sisesta esimene reaalarv");
        double firstRealn = Double.parseDouble(scanner.nextLine());

        System.out.println("Sisesta teine reaalarv");
        double secondRealn = Double.parseDouble(scanner.nextLine());

        System.out.printf("Arvude %.2f ja %.2f jagatis on %.2f.%n",
                firstRealn, secondRealn, firstRealn/secondRealn);

        // see tuleks parandada
        System.out.println(String.format(Locale.US.format"%.2f". ...args: firstRealn/secondRealn));

    }
}
