package com.valiit;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        try {
            FileWriter fileWriter = new FileWriter("output.txt", true);
            fileWriter.append("Minu nimi on Kea jejeje \r\n");
            fileWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        // 1. koduülesanne
        // Koosta täisarvude massiiv ja seejärel kirjuta faili kõik suuremad arvud kui kaks

        try {
            int[] massiiv = new int[]{1, -456, 34, 344, 2, -2, 1, 5, -12, 35, -66};
            FileWriter fileWriter = new FileWriter("outputNumbers.txt");
            for (int i = 0; i < massiiv.length; i++) {
                if (massiiv[i] > 2) {
                    fileWriter.write(massiiv[i] + "\r\n");
                }
            }
            fileWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        // 2. koduülesanne
        // Küsi kasutajalt kaks arvu, küsi seni, kuni mõlemad on korrektsed (on numbrid päriselt)
        // ja nende summa ei ole paarisarv.

        Scanner scanner = new Scanner(System.in);
        boolean numberError = false;
        int firstNumber = 0;
        int secondNumber = 0;
        boolean sumOfNumbers = false;

        do {
            do {
                try {
                    numberError = false;
                    System.out.println("Palun sisesta esimene arv.");
                    firstNumber = Integer.parseInt(scanner.nextLine());
                } catch (NumberFormatException e) {
                    numberError = true;
                    System.out.println("See ei ole number.");
                }
            } while (numberError);

            do {
                try {
                    numberError = false;
                    System.out.println("Palun sisesta teine arv.");
                    secondNumber = Integer.parseInt(scanner.nextLine());
                } catch (NumberFormatException e) {
                    numberError = true;
                    System.out.println("See ei ole number.");
                }
            } while (numberError);

            sumOfNumbers = false;
            int sumOfNumberstwo = firstNumber + secondNumber;
            if (sumOfNumberstwo % 2 != 0) {
                sumOfNumbers = true;
                System.out.println("Nende summa ei ole paarisarv. Palun proovi uuesti!");
            }
            else {
                System.out.println("Nende summa on paarisarv. Hea töö!");
            }
        } while (sumOfNumbers);


        System.out.println();

        // 3. Küsi kasutajal, mitu arvu ta tahab sisestada
        // Seejärel küsi ühekaupa kasutajalt need arvud
        // ning kirjuta nende arvude summa faili nii, et see lisatakse alati juurde


        System.out.println("Mitu arvu sa tahad sisestada?");
        int[] arvud = new int[Integer.parseInt(scanner.nextLine())];

        for (int i = 0; i < arvud.length; i++) {
            System.out.println("Sisesta arv");
            arvud[i] = Integer.parseInt(scanner.nextLine());
        }

        try {
            FileWriter fileWriter = new FileWriter("outputThird.txt", true);
            int sum = 0;

            for (int i = 0; i < arvud.length; i++) {
                sum += arvud[i];
            }
            fileWriter.append(sum + "\r\n"); // alternatiiv System.lineSeperator()
            fileWriter.close();
        } catch (IOException e) {
            System.out.println("Midagi läks valesti.");
        }

    }
}
