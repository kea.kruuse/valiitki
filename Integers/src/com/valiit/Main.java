package com.valiit;

public class Main {

    public static void main(String[] args) {
    	// võib lihtsalt deklareerida, ei pea kohe panema väärtust.
	    int number;
	    number = 888;
	    number = number +18;
	    number = number -290;
	    System.out.println(number);

	    int secondNumber = -7;
	    int multi = number*secondNumber;

	    System.out.println(secondNumber);
		System.out.println(number + secondNumber);
		System.out.println(number*secondNumber);

		System.out.printf("Arvude %d ja %d summa on %d ning korrutis %d.%n",
				number, secondNumber, number+secondNumber, multi);
		System.out.printf("Arvude %d ja %d summa on %d ning korrutis %d.%n",
				number, secondNumber, number+secondNumber, multi);
		// %n tekitab platvormispetsiifilise reavahetuse (Windows \n\r ja Linux/mac \n.
		// selle asemel saab kasutada ka System.lineSeparator()
		double a = 3;
		double b = 5;
		// String + number on alati String kui neid liidad, saad vastuseks Stringi
		// seetõttu tuleb panna sulgudesse. Tehete järjekord.
		System.out.println("Arvude summa on " + (a + b));
		System.out.println("Arvude jagatis on " + (a/b));
		int maxInt = 2147483647;
		int c = maxInt+1;
		int d = maxInt+2;
		// hakkab tagasi minema, kuna maxInt on maksimummahus
		short l = 32199;
		// long väärtust andes peaks lisama lõppu L
		long x = 88888888L;
		System.out.println(c);
		System.out.println(d);
		System.out.println(l);
    	System.out.println(x);
	}
}
