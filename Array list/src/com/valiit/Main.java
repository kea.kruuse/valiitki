package com.valiit;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Main {

    public static void main(String[] args) {

        // Generics: https://www.javatpoint.com/generics-in-java
        // Kohe kui ül on kustutamise või lisamise teema, tekib probleem, kuna juba alguses 0 ja siis ei tea, mis see 0 on


        int[] numbers = new int[5];

        // kui teen tühja massiivi, siis ta tegelikult ei ole tühi, sinna pannakse nullid sisse.

        for (int i = 0; i < numbers.length; i++) {
            System.out.println(numbers[i]);

        }

        System.out.println();

        // Lisa massiivi 5 numbrit

        numbers[0] = 2;
        numbers[1] = 0;
        numbers[2] = 3;
        numbers[3] = -4;
        numbers[4] = 8;

        // Eemalda siit teine nr

        numbers[0] = 0;


        for (int i = 0; i < numbers.length; i++) {
            System.out.println(numbers[i]);

        }

        numbers[3] = numbers[4];
        numbers[4] = 0;

        System.out.println();

        for (int i = 0; i < numbers.length; i++) {
            System.out.println(numbers[i]);

        }
        System.out.println();


        // sellist asja on üsna tüütu teha. Selleks ongi Array list

        // Tavalisse ArrayListi võin lisada ükskõik mis tüüpi elemente, aga elemente küsides pean teadma, mis tüüpi element on
        // ja kus täpselt asub ning pean siis selleks tüübiks küsimisel ka cast-ima e teisendama.

        // ArrayListi ei pea ütlema, mitu sinna mahub.

        List list = new ArrayList();

        list.add("tere");
        list.add(23);
        list.add(false);

        double money = 24.55;
        Random random = new Random();

        list.add(money);
        list.add(random);

        //       for (int i = 0; i < list.size(); i++) {
        //           System.out.println(list.get(i));
//
//        }

        int a = (int) list.get(1); // casting on siin vajalik st objekti ja mingi tüübi vahel

        String word = (String) list.get(0);

        String sentence = (String) list.get(0) + " hommikust";
        System.out.println(sentence);

        // on võimalik küsida, et mis tüüpi on

        if (String.class.isInstance(list.get(0))) ;
        // Peaks siis nii igaühte kontrollima

        // Kui võimalik, mitte ArrayListi kasutada selliselt
        //aga ArrayListi on võimalik teha ka nii, et ütled ära, mis seal on

        System.out.println();

        list.add(2, "tervitused");

        System.out.println();

       for (int i = 0; i < list.size(); i++) {
            System.out.println(list.get(i));
         }

       List otherList = new ArrayList();
       otherList.add(1);
        otherList.add(2);
        otherList.add(3);

       list.addAll(otherList);

       if (list.contains(money)){
           System.out.println("Money asub listis");
       }

        System.out.println();

        System.out.printf("Money asub listis indeksiga %d.%n", list.indexOf(money));
        System.out.printf("23 asub listis indeksiga %d.%n", list.indexOf(23));

        list.isEmpty(); // saab vaadata, et kas listis on kedagi või midagi.

        list.remove(money); // eemaldab listist money
      //  list.removeAll(); terve listi kustutus
        // list.set // asendab

        System.out.println();
        for (int i = 0; i < list.size(); i++) {
            System.out.println(list.get(i));
        }

        System.out.println();
        System.out.println("Listi suurus on " + list.size());


        // Üldiselt tänapäeva programmeerimises ArrayListi ei kasutata, sest võib vigu tekkida
        // Java eelis on, et on type-safe. ArrayListiga kaob see eelis aga ära. Võiks ikka olla iga asja jaoks oma tüüp
    }
}
