package com.valiit;

public class Main {

    public static void main(String[] args) {
		int a = -5;

			// Kui arv on neli, siis prindi välja
			// Muul juhul, kui arv on negatiivne, siis kontrolli, kas arv on suurem kui -kümme  ja prindi sellekohane tekst.
			// Muul juhul kontrolli, kas arv on suurem kahekümnest ja prindi sellekohane tekst.

		if(a==4){
			System.out.printf("Arv %d on neli.%n", a);
		}
		else if ((a < 0) && (a > -10)) {
			System.out.printf("Arv %d on negatiivne ja suurem kui -10.%n", a);
		}
		else if(a > 20){
			System.out.printf("Arv %d on suurem kui 20.%n", a);
		}

		// Võib panna ka if-e üksteise sisse.
		if(a==4){
			System.out.printf("Arv %d on neli.%n", a);
		}
		else {
			if (a < 0) {
				if (a > -10) {
					System.out.printf("Arv %d on negatiivne ja suurem kui -10.%n", a);
				} else {
					if (a < 20) {
						System.out.printf("Arv %d on suurem kui 20.%n", a);
					}
				}
			}
		}
    }
}
