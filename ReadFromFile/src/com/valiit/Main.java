package com.valiit;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class Main {

    public static void main(String[] args) {

        try {
            FileReader fileReader = new FileReader("C:\\Users\\opilane\\Documents\\input.txt");
            // fileReader.read() oskab lugeda ainult ühe sümboli kaupa. Selleks ongi BufferedReader, mis loeb rea kaupa
            // wrappimine tähendab, et üks komponent kasutab teist kompnenti enda siseselt
            BufferedReader bufferedReader = new BufferedReader(fileReader);

            // read.Line loeb iga kord välja kutsudes järgmise rea

            String line = bufferedReader.readLine();

            // kui readLine arvastab, et järgmist rida tegelikult ei ole, siis tagastab meetod null
            while (line != null){
                System.out.println(line);
                line = bufferedReader.readLine();
            }

            // do-while'ga see probleem, et ta läheb ka esimene kord tsükklisse sisse

            //do {
            //    line = bufferedReader.readLine();
            //    if(line != null) {
            //        System.out.println(line);
            //    }
            // } while (line != null);

            bufferedReader.close();
            fileReader.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
