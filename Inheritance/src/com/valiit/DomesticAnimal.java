package com.valiit;

public class DomesticAnimal extends Animal {
    public int feededPerDay = 1;

    public int getFeededPerDay() {
        return feededPerDay;
    }

    public void setFeededPerDay(int feededPerDay) {
        this.feededPerDay = feededPerDay;
    }

    public void askForFood(String foodName){
        System.out.printf("Andsin inimesele märku, et tahan süüa %s.%n", foodName);
    }


}
