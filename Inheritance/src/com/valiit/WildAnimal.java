package com.valiit;

public class WildAnimal extends Animal {
    private boolean isAfraidofHumans = true;

    public boolean isAfraidofHumans() {
        return isAfraidofHumans;
    }

    public void setAfraidofHumans(boolean afraidofHumans) {
        isAfraidofHumans = afraidofHumans;
    }

    public void changeTerritory(String newTerritory){
        System.out.printf("Läksin ise teise kohta elama. Uus territoorium on %s. %n", newTerritory);
    }


}
