package com.valiit;

public class Main {

    public static void main(String[] args) {

        Cat mustik = new Cat();
        mustik.setName("Mustik");
        mustik.setBreed("Siiam");
        mustik.setAge(2);
        mustik.setWeight(4.1);

        mustik.printInfo();
        System.out.println();
        mustik.eat();
        System.out.println();

        Cat nola = new Cat();
        nola.setName("Nola");
        nola.setBreed("Vene triibuline");
        nola.setAge(3);
        nola.setWeight(5.1);

        nola.printInfo();
        System.out.println();
        nola.eat();
        System.out.println();

        Dog sass = new Dog();
        sass.setName("Sass");
        sass.setAge(5);
        sass.setBreed("krants");
        sass.setWeight(6.2);

        sass.printInfo();
        System.out.println();
        sass.eat();
        System.out.println();

        nola.catchMouse();
        System.out.println();
        sass.playWithCat(nola);
    }
}
