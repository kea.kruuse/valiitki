package com.valiit;

public class Wolf extends CarnivoreAnimal {
    private boolean hasKilledAMoose;

    public boolean isHasKilledAMoose() {
        return hasKilledAMoose;
    }

    public void setHasKilledAMoose(boolean hasKilledAMoose) {
        this.hasKilledAMoose = hasKilledAMoose;
    }

    public void howl(){
        System.out.println("Auuuuuuuuu!");
    }



}
