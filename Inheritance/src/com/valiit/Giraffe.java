package com.valiit;

public class Giraffe extends HerbivoreAnimal {
    public double lengthOfNeck;

    public double getLengthOfNeck() {
        return lengthOfNeck;
    }

    public void setLengthOfNeck(double lengthOfNeck) {
        this.lengthOfNeck = lengthOfNeck;
    }

    public void enjoyTheView(){
        System.out.println("Siit kõrgelt on ilus vaade.");
    }

}
