package com.valiit;

public class Pig extends FarmAnimal {
    private boolean isPink;

    public boolean isPink() {
        return isPink;
    }

    public void setPink(boolean pink) {
        isPink = pink;
    }

    public void röhitseb(){
        System.out.println("Röh-röh!");
    }

}
