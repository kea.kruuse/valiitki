package com.valiit;

public class Pet extends DomesticAnimal {
    private double IQ;

    public double getIQ() {
        return IQ;
    }

    public void setIQ(double IQ) {
        this.IQ = IQ;
    }

    public void playWithAnotherDomesticAnimal(DomesticAnimal anotherDomesticAnimal){
        System.out.printf("Mängisin teise loomaga %s.%n", anotherDomesticAnimal.getName());
    }



}
