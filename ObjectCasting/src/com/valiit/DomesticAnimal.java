package com.valiit;

public class DomesticAnimal extends Animal {
    private int feededPerDay = 1;

    public int getFeededPerDay() {
        return feededPerDay;
    }

    public void setFeededPerDay(int feededPerDay) {
        this.feededPerDay = feededPerDay;
    }

    public void askForFood(String foodName){
        System.out.printf("Andsin inimesele märku, et tahan süüa %s.%n", foodName);
    }

    @Override
    public void printInfo(){
        super.printInfo();
        System.out.printf("Päevas tuleb süüa anda %d korda %n", getFeededPerDay());
    }

}
