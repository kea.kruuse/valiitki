package com.valiit;

public class Horse extends HerbivoreAnimal {
    private boolean isAPony;

    public boolean isAPony() {
        return isAPony;
    }

    public void setAPony(boolean APony) {
        isAPony = APony;
    }

    public void jumpOverFence(){
        System.out.println("Hüppasin üle tõkke.");
    }

}
