package com.valiit;

public class HerbivoreAnimal extends WildAnimal {
    private double runningSpeed;

    public double getRunningSpeed() {
        return runningSpeed;
    }

    public void setRunningSpeed(double runningSpeed) {
        this.runningSpeed = runningSpeed;
    }

    public void layDownAndEatGrass() {
        System.out.println("Lebasin maha ja sõin muru.");
    }


}
