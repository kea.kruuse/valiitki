package com.valiit;

public class Animal {
    private String name;
    private String breed;
    private int age;
    private double weight;
    private boolean isAlive = true;


    public boolean isAlive() {
        return isAlive;
    }

    public void setAlive(boolean alive) {
        isAlive = alive;
    }

    public String getName() {
            return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBreed() {
        return breed;
    }

    public void setBreed(String breed) {
        this.breed = breed;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public void eat() {
        System.out.println("Söön toitu.");
    }

    public void printInfo() {
        System.out.println("Info: ");
        System.out.printf("Nimi: %s. %n", getName());
        System.out.printf("Tõug: %s. %n", getBreed());
        System.out.printf("Vanus: %d aastat. %n", getAge());
        System.out.printf("Kaal: %.1f kg. %n", getWeight());
        System.out.println();
    }

   /* Inheritance is an important pillar of OOP(Object Oriented Programming). It is the mechanism in java by which one class is allow
   to inherit the features(fields and methods) of another class.

    Important terminology:

    Super Class: The class whose features are inherited is known as super class(or a base class or a parent class).

    Sub Class: The class that inherits the other class is known as sub class(or a derived class, extended class, or child class).
    The subclass can add its own fields and methods in addition to the superclass fields and methods.

    Reusability: Inheritance supports the concept of “reusability”, i.e. when we want to create a new class and
    there is already a class that includes some of the code that we want, we can derive our new class from the existing class.
    By doing this, we are reusing the fields and methods of the existing class.
*/


}
