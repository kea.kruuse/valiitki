package com.valiit;

public class Dog extends Pet {

    private boolean hasTail = true;


    public boolean isHasTail() {
        return hasTail;
    }

    public void setHasTail(boolean hasTail) {
        this.hasTail = hasTail;
    }
    

    public void playWithCat(Cat cat) {
        System.out.printf("Mängin kassiga %s. %n", cat.getName());
    }

    @Override
    public void eat() {
        System.out.println("Närin konti."); // method overriding
    }

    @Override
    public int getAge() {
        if (super.getAge() == 0){
            return 1;
        }
        return super.getAge();
    }
}
