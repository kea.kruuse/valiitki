package com.valiit;

public class CarnivoreAnimal extends WildAnimal {
    private int numberOfTeeth;

    public int getNumberOfTeeth() {
        return numberOfTeeth;
    }

    public void setNumberOfTeeth(int numberOfTeeth) {
        this.numberOfTeeth = numberOfTeeth;
    }

    public void eatAnotherAnimal(Animal anotherAnimal){
        System.out.printf("Sõin ära looma %s.%n", anotherAnimal.getName());
        anotherAnimal.setAlive(false);
    }




}
