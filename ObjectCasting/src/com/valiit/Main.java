package com.valiit;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {
	    int a = 100;
	    short b = (short) a;

	    a= b;

	    // väiksemat tüüpi saab suuremasse alati panna, aga mitte vastupidi.

        double c = a;
        a = (int)c;

        // iga kassi võib võtta kui looma
        Animal animal = new Cat();

        // Iga loom ei ole kass.
        Cat cat = (Cat) animal;

        List<Animal> animals = new ArrayList<>();

        Dog puppy = new Dog();
        Cow mummu = new Cow();

        animals.add(mummu);
        animals.add(puppy);
        puppy.setName("Naka");
        animals.add(new Lion()); // saab ka otse luua ja sisse panna
        animals.add(new Wolf());
        animals.add(new Pet());


        // siia saab lisada kõiki, kes pärinevad Animalist

        // Kutsu kõikide loomade printinfo välja

        animals.get(4).setName("Aua");
        animals.get(animals.size()-1).setName("Piup");
        animals.add(new Cat());
        animals.get(animals.size()-1).setName("Mustikas");
// For tsükkel:

 //       for (int i = 0; i < animals.size(); i++) {
//            animals.get(i).printInfo();
//        }
// For-each tsükkel
        // Vasakul pool mis tüüpi on ja nimi
        // Paremal pool, mis nimekirja(kollektsiooni) läbi käib
        for (Animal animalTwo: animals) {
            animalTwo.printInfo();
        }

        Animal secondAnimal = new Dog();
        ((Dog) secondAnimal).playWithCat(new Cat()); //Peab castima ja sulud ümber
        ((Dog) secondAnimal).setFeededPerDay(2);


        Wolf myFavWolf = new Wolf();
        animals.add(myFavWolf);

        List<Animal> myFavAnimals = new ArrayList<>();
        myFavAnimals.add(myFavWolf);

        Wolf hisWolf = myFavWolf;

        List<Animal> hisFavAnimals = new ArrayList<>();
        hisFavAnimals.add(hisWolf);

        myFavWolf.setAge(5);
        System.out.println();

        // saab hoida üht ja sama objekti eri nimekirjades
        System.out.println(hisFavAnimals.get(0).getAge());
        System.out.println(myFavAnimals.get(0).getAge());
        System.out.println(myFavWolf.getAge());
    }
}
