package com.valiit;

import java.util.Scanner;
// see ülemine oli vaja importida, selleks kirjuta Scanner ja siis tab ja alt+enter
public class Main {

    public static void main(String[] args) {
// loome scanner tüübist objekti nimega scanner, mille kaudu saab kasutaja sisendit lugeda
        Scanner scanner = new Scanner( System.in );

        System.out.println("Tere! Mis on sinu eesnimi?");
        String firstName = scanner.nextLine();

        System.out.println("Mis on sinu perekonnanimi?");
        String lastName = scanner.nextLine();

        System.out.println("Mis brand on su auto?");
        String car = scanner.nextLine();

        System.out.println("Milline ilm on õues?");
        String weather = scanner.nextLine();

        System.out.println("Tere, " + firstName + " " + lastName + "! Tore, et sõidad " + car +"ga. Ilm õues on " + weather +"!");


        // see järgnev osa jäta meelde
        System.out.printf("Tere, %s %s! Tore, et sõidad %sga. Ilm õues on %s!\n",
                firstName, lastName, car, weather);

        StringBuilder builder = new StringBuilder();
        builder.append("Tere, ");
        builder.append(firstName);
        builder.append(" ");
        builder.append(lastName);
        builder.append("! Tore, et sõidad ");
        builder.append(car);
        builder.append("ga. ");
        builder.append("Ilm õues on ");
        builder.append(weather);
        builder.append("!");
        builder.toString();
        String lause = builder.toString();
        System.out.println(lause);

        // nii System.out.println kui ka printf kasutavad oma sisemuselt String.builderit - mälule parem
        String test = String.format("Tere, %s %s! Tore, et sõidad %sga. Ilm õues on %s!",
                firstName, lastName, car, weather);
        System.out.println(test);

    }
}
