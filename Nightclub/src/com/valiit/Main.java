package com.valiit;


import java.io.PrintStream;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        // Kõigepealt küsitakse külastaja nime.
        // Kui nimi on listis, siis öeldakse kasutajale
        // "tere tulemast, xx"
        // ja küsitakse külastaja vanust
        // Kui kasutaja on alaealine
        // siis teavitatakse teda, et sorry, sisse ei saa
        // Muul juhul öeldakse, et "Tere tulemast klubisse!"

        // Kui kasutaja ei olnud listis
        // küsitakse kasutajalt ka perekonnanimi
        // kui perekonnanimi on listis, siis öeldakse "tere tulemast, xx xx"
        // muul juhul öeldakse, et ma ei tunne sind.

        String firstName = "Kea";
        String lastName = "Kruuse";

        Scanner scanner;
        scanner = new Scanner(System.in);

        System.out.println("Tere, mis on sinu eesnimi?");
        String eesnimi = scanner.nextLine();

        if (eesnimi.toLowerCase().equals(firstName.toLowerCase())) {
            System.out.printf("Tere tulemast, %s!%n", eesnimi.toUpperCase());
            System.out.printf("Kui vana sa oled?%n");
            int firstAge = Integer.parseInt(scanner.nextLine());
            if (firstAge >= 18){
                System.out.printf("Tere tulemast klubisse!");
            }
            else {
                System.out.printf("Sa oled liiga noor! Mine koju.");
            }
            }
        else{
            System.out.printf("%s, mis on sinu perekonnanimi?%n", eesnimi);
            String perenimi = scanner.nextLine();
            if (perenimi.toLowerCase().equals(lastName.toLowerCase())){
                System.out.printf("Oo sa oled %s sugulane!%n", firstName);
                System.out.printf("Kui vana sa oled?%n");
                int secondAge = Integer.parseInt(scanner.nextLine());
                if (secondAge >= 18){
                    System.out.printf("Tere tulemast klubisse!");
                }
                else {
                    System.out.printf("Sa oled liiga noor! Mine koju.");
                }
            }
            else {
                System.out.println("Kahjuks sind pole listis. Mine osta pilet.");
            }
        }
        }
    }