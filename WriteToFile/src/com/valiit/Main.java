package com.valiit;

import java.io.FileWriter;

public class Main {

    public static void main(String args[]){
        // Faili saab javas kirjutada väga erinevat moodi. Nt guugelda. "Java write to file"
        // https://www.baeldung.com/java-write-to-file
        // Boolean append - kas tahad juurde kirjutada või uut faili
        // Java IO-exception. On terve hunnik erinevvaid exceptione.
        // vt nt https://programming.guide/java/list-of-java-exceptions.html
        // Kuskil peab exceptiomiga tegelema - kas otse main'is või kuskil mujal (ala throw)
        // alternatiiv throw'le on kohe main'is sellega tegeleda. Selleks on try-catch.
        // Kaks kaldkriipsu on asukoht, üks kaldkriips, et hakkab midage excape'ima
        // Exceptionitel on suguluspuud, alagrupid, ala üldine veagrupp, mille all täpsem
        // Nt kui paned, et püüa IO Exception kinni, siis ta püüab kinni ka selle ala-exceptionid magu nt FileNotFoundException
        // Try plokis otsitakse või oodatakse exceptioneid ehk erindeid/erandeid/vigu
        // catch-plokis püütakse kinni kindlat tüüpi exception või kõik exceptionid, mis pärinevad
        // antud exceptionist.
        // on hierarhia, kuidas exceptionid on loodud, nad kuuluvad kategooriatesse.

        try{
            // Filw-writer on klass, mis tegeleb failikirjutamisega.
            // Põhimõtteliselt sellest klassist objekti loomisel antakse talle ette faili asukoht
            // faili asukoht võib olla ainult faili nimega kirjutatud, nt output.txt
            // või täispika asukohaga, nt e:\\output.txt või ala c:\\users\\opilane\\output.txt (alati kaks kaldkriipsu!
            // Kui ei ütle asukohta kirjutatakse faili, mis asub samas kaustas, kus meie main.class

            // see kood kirjutab iga kord üle
            FileWriter fileWriter = new FileWriter("c:\\users\\opilane\\documents\\output.txtt");
            fileWriter.write("Minu nimi on Kea jejeje \r\n" +
                    "Pauraunjdnfjdhrf!");
            fileWriter.write(String.format("Minu nimi on Kea jejeje%n");
            fileWriter.write("Minu nimi on Kea jejeje" + System.lineSeparator());
            fileWriter.close(); // kui seda ei tea, siis operatsioonisüsteem ei tea, et oled lõpetanud.

        }catch(Exception e){
            // e.printStackTrace();
            // see teebki selle pika joru alla. printStackTrace prindib välja meetodite väljakutsumise hierarhia/ajaloo
            // kood võib väga sügavale minna, seetõttu se eon hea, sest näitab, kus tekkis viga ja saab otse seda trace'ida
            // see on info, mis ei tohiks lõpptarbijani jõuda
            // lõppkasutajale ei taha seda pikka joru näidata, aga võid mõelda konkreetse veateate.
            // ala võid panna ka, et "Hetkel kahjuks süsteem ei tööta"
            System.out.println("Viga: antud failile ligipääs ei ole võimalik.");
        }
    }

   // static double divide(int g, int d) throws ArithmeticException {
    //    return (double) g/d;

}