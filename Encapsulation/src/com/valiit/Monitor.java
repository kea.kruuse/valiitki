package com.valiit;

// num on tüüp, kus saab defineerida erinevaid lõplikke valikuid
// Tegelikult salvestatakse enum alati int-na. Javas see veidi keerulisem.
// Kui soovime enumis kasutada mingeid teisi numbreid: vt http://tutorials.jenkov.com/java/enums.html

enum Color {
    BLACK,
    WHITE,
    GRAY,
    BLUE,
    GREEN,
    YELLOW,
    RED
}

enum ScreenType {
    LCD,
    TFT,
    OLED,
    AMOLED
}

// private tähendab, et need parameetrid pole väljapoole kättesaadavad
// Getter ja setter. Saab privaatmuutuja väärtust seadistada ja küsida

// Igal muutujatüübil on oma vaikeväärtused.
// Int vaikeväärtus 0;
// Booleani vaikeväärtus on false;
// double vaikeväärtus on 0.0;
// Stringi vaikeväärtus on null
// Objektide (Monitor, FileWriter) vaikeväärtus on null
// Massiivi vaikeväärtus: int[] arvud on null
// floatil on 0.0f

public class Monitor {

    private String manufacturer = "Tootja puudub";
    private double diagonal; // tollides
    private Color color;
    private ScreenType screenType;
    private int year = 2000;


    public int getYear() { // yearil on ainult get meetod ehk seda aastat ei olegi võimalik enam kunagi muuta
        return year;
    }

    public double getDiagonal(){
        if(diagonal == 0){
            System.out.println("Diagonaal on seadistamata.");
        }
        return diagonal; // pmst võib ka siia panna this.diagonal, aga kuna ainult üks siin, siis otsib klassist
    }

    public void setDiagonal(double diagonal) { // kui tagastama ei pea, ainult seadistad, pane "void"
        if(diagonal < 0){
            System.out.println("Diagonaal ei saa olla negatiivne.");
        }
        else if (diagonal > 100) {
            System.out.println("Diagonaal ei saa olla suurem kui 100\"");
        }
        else{
            this.diagonal = diagonal; // this tähistab seda konkreetset objekti
        }
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    // Ära luba panna monitore, mille tootja on Huawei
    // Ära luba ka panna sellist nime, kus on lihtsalt tühi auk
    public void setManufacturer(String manufacturer) {
        if (manufacturer == null || manufacturer.equals("Huawei")){ //null-i kontroll peab alati esimene olema
            this.manufacturer = "Tootja puudub";
        }
        else if (manufacturer.equals(" ") || manufacturer.equals("0")){
            System.out.println("Pole sobilik nimi.");
            this.manufacturer = "Tootja puudub";
        }
        else {
            this.manufacturer = manufacturer;
        }
    }

    // Kui ekraanitüüp seadistamata, tagastab LCD
    public ScreenType getScreenType() {
            if (screenType == null) {
                return ScreenType.LCD;
            }
            return screenType;
        }

    public void setScreenType(ScreenType screenType) {
        this.screenType = screenType;
    }


    public void printInfo() {

        System.out.println("Monitori info:");
        System.out.printf("Tootja: %s%n", getManufacturer());
        System.out.printf("Diagonaal: %.1f%n", getDiagonal()); // ümardab ühe kohani
        System.out.printf("Värv: %s%n", getColor());
        System.out.printf("Ekraani tüüp: %s%n", getScreenType());
        System.out.println("Aasta: " + getYear());
        System.out.println();
    }

    // Meetod, mis tagastab ekraani diagonaali inches sentimeetiteks

    public double diagonalinCentimeters() {
            return diagonal * 2.54f;
    }
}
