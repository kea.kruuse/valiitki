package com.valiit;

public class Main {

    public static void main(String[] args) {
	// A language mechanism for restricting direct access to some of the object's components.
    // A language construct that facilitates the bundling of data with the methods (or other functions) operating on that data.
    // ATM mõttes saab nt balance suureneda ainult siis kui pannakse raha sisse

        Monitor firstMonitor = new Monitor();
        firstMonitor.setDiagonal(-10);
        System.out.println(firstMonitor.getDiagonal());

        firstMonitor.setDiagonal(200);
        System.out.println(firstMonitor.getDiagonal());

        System.out.println(firstMonitor.getYear());

        // get ja set meetodid selleks: 1) saan piirangud panna, 2) saan üldse keelata muutmise ära
        // võib ka teistpidi, nt saab ainult seadistada tootjat, aga küsida ei saa.

        firstMonitor.setManufacturer(" ");
        System.out.println(firstMonitor.getManufacturer());

    }
}
