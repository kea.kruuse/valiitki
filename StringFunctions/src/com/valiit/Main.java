package com.valiit;

public class Main {

    private static boolean input;

    public static void main(String[] args) {
        String sentence = "Elas metsas Mutionu keset kuuski noori vanu.";

        // Sümbolite indeksid tekstis algavad samamoodi indeksiga 0
        // nagu massiivis.

        // Leia üles esimene tühik, mis on tema indeks.
        int spaceIndex = sentence.indexOf(" ");
        // indexOf tagastab -1, kui sellist sümbolit ei ole.

        // indexOf-iga saab teada, kus sümbol Stringis asub või kas sellist sümbolit üldse on.
        System.out.println(spaceIndex);
        System.out.println();

        int secondSpaceIndex = sentence.indexOf(" ", spaceIndex +1);
        System.out.println(secondSpaceIndex);
        System.out.println();

        //prindi välja kõikide tühikute indeks;

        int spaceIndexTwo = sentence.indexOf(" ");
        while(spaceIndexTwo != -1){
            System.out.println(spaceIndexTwo);
            spaceIndexTwo = sentence.indexOf(" ", spaceIndexTwo +1);
        }
        System.out.println();

        // prindi tühikute kohad välja tagantpoolt alates
        spaceIndexTwo =  sentence.lastIndexOf(" ");
        while(spaceIndexTwo != -1){
            System.out.println(spaceIndexTwo);
            spaceIndexTwo = sentence.lastIndexOf(" ", spaceIndexTwo - 1);
        }
        System.out.println();

        // Prindi välja lause esimesed neli tähte.
        // eeldab, et tead, kust lause algab ja kus lõppeb.
        String part = sentence.substring(5,11); // alguse ja lõpu indeks, aga lõppu pole k.a

        // Anna lause esimene sõna
        part = sentence.substring(0,sentence.indexOf(" "));
        System.out.println(part);

        // Leia teine sõna
        int indexTuhik = sentence.indexOf(" ", sentence.indexOf(" ")+1);
        part = sentence.substring(sentence.indexOf(" ")+1, indexTuhik);
        System.out.println(part);

        // Leia esimene k tähega sõna
        String firstLetter = sentence.substring(0, 1);
        int kIndex;
        int tuhik;
        if(firstLetter.toLowerCase().equals("k")) { // ehk kontrollime, kas sõna alguses
            tuhik = sentence.indexOf(" ");
            part = sentence.substring(0, tuhik);
            System.out.println(part);
        }
        else{ // kui k täht ei ole sõna alguses
            kIndex = sentence.toLowerCase().indexOf(" k") + 1;
            tuhik = sentence.indexOf(" ", kIndex);
            part = sentence.substring(kIndex, tuhik);
            System.out.println(part);
        }

        System.out.println();
        // Leia, mitu sõna lauses on.

        int counter = 0;
        spaceIndexTwo = sentence.indexOf(" ");
        while(spaceIndexTwo != -1){
            counter++;
            spaceIndexTwo = sentence.indexOf(" ", spaceIndexTwo +1);
        }
        System.out.println(counter +1);


        // Leia, mitu k-tähega algavat sõna lauses on.

    }
}
